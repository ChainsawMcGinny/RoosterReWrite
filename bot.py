import logging
import sys
import traceback

import aiohttp
import aioredis
import nextcord
import pendulum
from nextcord.ext import commands
from cogs.utils import context
from cogs.utils.esi import ESI
from cogs.utils.zkillsocket import zKillSocket
from multiprocessing import Queue

try:
    import config
except ImportError:
    print("Config not found, have you copied over the example settings?")
    sys.exit(1)

description = """
Rooster knows all...
"""

log = logging.getLogger(__name__)

initial_cogs = (
    "cogs.about",
    "cogs.who",
    "cogs.insurance",
    # "cogs.market",
    "cogs.mute",
    "cogs.reminder",
    "cogs.time",
    "cogs.thera",
    "cogs.trivia",
    "cogs.weather",
    "cogs.where",
    "cogs.joinchannel",
    "cogs.route",
    "cogs.memberjoin",
    "cogs.eft",
    "cogs.killstream",
    "cogs.units",
    "cogs.esiprice",
)


class Rooster(commands.Bot):
    def __init__(self):
        intents = nextcord.Intents.default()
        intents.members = True
        super().__init__(
            command_prefix="!",
            description=description,
            help_command=commands.DefaultHelpCommand(dm_help=True),
            help_attrs=dict(hidden=True),
            intents=intents
        )
        self.kill_queue = Queue()
        self.zk = zKillSocket(self.kill_queue)
        self.zk.start()
        self.redis = None
        self.redis = self.loop.run_until_complete(
            aioredis.create_pool((config.redis_host, config.redis_port), minsize=5, maxsize=10))
        print('redis pool started', self.redis)
        self.client_id = config.client_id
        self.session = aiohttp.ClientSession(loop=self.loop)
        self.esi = ESI()

        for cog in initial_cogs:
            try:
                self.load_extension(cog)
            except Exception as e:
                print(f"Failed to load cog {cog}", file=sys.stderr)
                traceback.print_exc()

    async def on_comand_error(self, ctx, error):
        if isinstance(error, commands.NoPrivateMessage):
            await ctx.author.send("This command cannot be used in Private Messages")
        elif isinstance(error, commands.DisabledCommand):
            await ctx.author.send("Sorry, this command is disabled.")
        elif isinstance(error, commands.CommandInvokeError):
            print(f"In {ctx.command.qualified_name}", file=sys.stderr)
            traceback.print_tb(error.original.__traceback__)

    async def on_ready(self):
        if not hasattr(self, "currentuptime"):
            self.currentuptime = pendulum.now(tz="UTC")
        print("Ready")
        print(nextcord.__version__)

    async def process_commands(self, message):
        ctx = await self.get_context(message, cls=context.Context)

        if ctx.command is None:
            return
        await self.invoke(ctx)

    async def on_message(self, message):
        if message.author.bot:
            return
        await self.process_commands(message)

    async def on_resumed(self):
        print("Resumed...")

    async def on_command_error(self, ctx, error):
        if isinstance(error, commands.BadArgument):
            await ctx.send(error)
        elif isinstance(error, commands.MissingRequiredArgument):
            await ctx.send(error)
        elif isinstance(error, commands.NoPrivateMessage):
            await ctx.send(error)
        elif isinstance(error, commands.CommandInvokeError):
            return await ctx.send(error)
        elif isinstance(error, commands.BotMissingPermissions):
            await ctx.send(
                f"Sorry, I don't have the required permissions to do that here:\n{error.missing_perms}"
            )
        elif isinstance(error, commands.MissingPermissions):
            await ctx.send(f"Sorry, you do not have permission to do that here.")
        elif isinstance(error, commands.NotOwner):
            await ctx.send(error)
        elif isinstance(error, commands.CommandOnCooldown):
            await ctx.message.add_reaction(chr(0x274C))

    def run(self):
        super().run(config.token, reconnect=True)

    @property
    def config(self):
        return __import__("config")
