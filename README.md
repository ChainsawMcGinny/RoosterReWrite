# Rooster
![rooster](https://j4lp.space/static/images/logohires.png)


Rooster is a multifunction discord bot tailored specifically for eve-online corporations.

# Running

It preferrable that you invite my hosted version of rooster to your server with the !invite link, however if you must run your own local bot:

# Dependencies

Python 3.6 or higher and the header files (generally python3.6-dev and/or libpython3.6-dev) are required (If you are on Ubuntu 16.04 or older, you may need to add a launchpad repository to your apt config to get these as they
are not included in the the default repositories - example for python3.6 on xenial: https://launchpad.net/~jonathonf)

Redis
(optional but really reccomended) redis-tools

These can be installed via your favorite (or not favorite) package manager `apt-get install redis-server redis-tools python3.6 python3.6-dev build-essential`

# Configuration

Copy config.py.example to config.py, edit appropriately.

create a pyvenv using `python3.6 -m venv /path/to/venv/`

activate your venv `source /path/to/venve/bin/activate`

make sure your pip is up to date: `pip install pip --upgrade`

install requirements based in requirements.txt to your venv `pip install -r requirements.txt`

then simply run `python launcher.py` and enjoy.

Rooster is made with love for Fweddit and friends by Chainsaw McGinny.

Special thanks to Sergei Alexeyev for a baller api at fweddit.space (previously evehound) and for constant bugfixing and stress testing.
