import logging
import traceback

from opencage.geocoder import OpenCageGeocode
from darksky import forecast
from nextcord.ext import commands

import config
from nextcord import embeds, Color

log = logging.getLogger(__name__)


class Weather(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.geocoder = OpenCageGeocode(key=config.opencage_key)

    @commands.command(pass_context=True)
    async def weather(self, ctx, *, city):
        """
        Gives the current weather of a location
        """
        if not city:
            raise commands.MissingRequiredArgument()

        await ctx.trigger_typing()
        # address = self.gmaps.geocode(city)
        address = self.geocoder.geocode(city)
        if not address:
            raise commands.BadArgument("City Not Found")

        # lat = address[0]['geometry']['location']['lat']
        # lng = address[0]['geometry']['location']['lng']
        lat = address[0]["geometry"]["lat"]
        lng = address[0]["geometry"]["lng"]

        with forecast(config.weather_api, lat, lng) as myforecast:
            embed = embeds.Embed(
                title=address[0]["formatted"],
                url=f"https://darksky.net/forecast/{lat},{lng},",
                description=myforecast.daily.summary,
            )
            condition = self.weather_condition_image(myforecast.currently.icon)
            embed.set_thumbnail(url=condition)
            embed.set_footer(text="https://darksky.net/poweredby/")
            embed.add_field(
                name="Current Conditions:",
                # imma leave this .format here because gawd damn
                value="{}\n{}F/{:.1f}C (feels like {}F/{:.1f}C) at {}% Humidity".format(
                    myforecast.currently.summary,
                    myforecast.currently.temperature,
                    (myforecast.currently.temperature - 32) * (5 / 9),
                    myforecast.currently.apparentTemperature,
                    (myforecast.currently.apparentTemperature - 32) * (5 / 9),
                    int(myforecast.currently.humidity * 100),
                ),
            )

            if hasattr(myforecast, "alerts"):
                embed.color = Color.red()
                for alert in myforecast.alerts:
                    embed.add_field(
                        name=alert["severity"], value=alert["title"], inline=False
                    )

            return await ctx.send(embed=embed)

    def weather_condition_image(self, condition):
        """
        icons for weather thanks to https://www.dr-lex.be/software/darksky-icons.html
        uploaded to imgur for hardcoded goodness
        """
        if condition == "clear-day":
            return "https://i.imgur.com/ebBKojX.png"
        if condition == "clear-night":
            return "https://i.imgur.com/OZ2bhJw.png"
        if condition == "rain":
            return "https://i.imgur.com/ZxYcTvw.png"
        if condition == "snow":
            return "https://i.imgur.com/q1r67Lz.png"
        if condition == "sleet":
            return "https://i.imgur.com/0NG3rKH.png"
        if condition == "wind":
            return "https://i.imgur.com/88CjA2l.png"
        if condition == "fog":
            return "https://i.imgur.com/ZqMpsQ6.png"
        if condition == "cloudy":
            return "https://i.imgur.com/cbvq4Si.png"
        if condition == "partly-cloudy-day":
            return "https://i.imgur.com/SlGJVoL.png"
        if condition == "partly-cloudy-night":
            return "https://i.imgur.com/J4Y1VgH.png"


def setup(bot):
    bot.add_cog(Weather(bot))
