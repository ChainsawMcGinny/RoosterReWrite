import logging
import traceback

from nextcord.embeds import Embed
from nextcord import Color
from nextcord.ext import commands

log = logging.getLogger(__name__)


class Where(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def where(self, ctx, *, location):
        """
        Lookup basic info on systems, constellations, or regions
        """
        if not location:
            raise commands.MissingRequiredArgument()

        await ctx.trigger_typing()
        # search for solar system
        try:
            location_lookup = await self.bot.esi.search("solar_system", location)
            system_id = location_lookup["solar_system"][0]
        except KeyError:
            pass
        else:
            await self.get_system_info(system_id, ctx)
            return

        # search for constellation
        try:
            location_lookup = await self.bot.esi.search("constellation", location)
            const_id = location_lookup["constellation"][0]
        except KeyError:
            pass
        else:
            await self.get_const_info(const_id, ctx)
            return

        # search for region
        try:
            location_lookup = await self.bot.esi.search("region", location)
            region_id = location_lookup["region"][0]
        except KeyError:
            pass
        else:
            await self.get_region_info(region_id, ctx)
            return

        # hail mary loose search for a system
        try:
            location_lookup = await self.bot.esi.loose_search("solar_system", location)
            system_id = location_lookup["solar_system"][0]
        except KeyError:
            pass
        else:
            await self.get_system_info(system_id, ctx)
            return

        # none of the above found anything, abort abort abort
        return await ctx.send("No location found with that name")

    async def get_system_info(self, system_id, ctx):
        # get our system sheet
        system_info = await self.bot.esi.system_info(system_id=system_id)

        # get the constellation info, which has the region
        const_info = await self.bot.esi.constellation_info(
            constellation_id=system_info["constellation_id"]
        )

        # and the region info, esi is a silly place
        region_info = await self.bot.esi.region_info(region_id=const_info["region_id"])

        # get last hour kills
        kills = await self.bot.esi.system_kills()

        # ITERATE BECAUSE WHY WOULD WE USE SYSTEM ID AS A KEY I DUNNO
        ship_kills, npc_kills, pod_kills = 0, 0, 0
        for system in kills:
            if system["system_id"] == system_id:
                ship_kills = system["ship_kills"]
                npc_kills = system["npc_kills"]
                pod_kills = system["pod_kills"]
                break

        jump_info = await self.bot.esi.system_jumps()

        jumps = 0
        for system in jump_info:
            if system["system_id"] == system_id:
                try:
                    jumps = system["ship_jumps"]
                    break
                except KeyError:
                    pass

        if system_info["security_status"] <= 0:
            # get sov info
            sov_info = await self.bot.esi.sov_map()
            # get sov info if exists
            for system in sov_info:
                if system["system_id"] == system_id:
                    try:
                        alliance_id = system["alliance_id"]
                    except KeyError:
                        alliance_id = None
        else:
            alliance_id = None

        if alliance_id:
            alliance_sheet = await self.bot.esi.alliance_sheet(alliance_id=alliance_id)

            # get ADM if an alliance owns system
            adm_info = await self.bot.esi.system_adm()

            for system in adm_info:
                if system["solar_system_id"] == system_id:
                    if system["structure_type_id"] == 32226:
                        adm = system["vulnerability_occupancy_level"]

        embed = Embed(
            title=system_info["name"],
            url=f'http://evemaps.dotlan.net/system/{system_info["name"].replace(" ", "_")}',
        )

        embed.add_field(name="Region", value=region_info["name"], inline=True)
        embed.add_field(name="Constellation", value=const_info["name"], inline=True)
        embed.add_field(
            name="SecStatus", value=round(system_info["security_status"], 2)
        )
        embed.add_field(name="Jumps (1h)", value=jumps)
        embed.add_field(
            name="Kills (1h)",
            value=f"Ships: {ship_kills}\nPods: {pod_kills}\nNPC: {npc_kills}",
        )

        if alliance_id:
            embed.add_field(name="Current Owner", value=alliance_sheet["name"])
            embed.add_field(name="Current ADM", value=adm)
            embed.set_thumbnail(
                url=f"https://image.eveonline.com/Alliance/{alliance_id}_128.png"
            )
        else:
            embed.set_thumbnail(
                url="https://image.eveonline.com/Alliance/434243723_128.png"
            )
            embed.add_field(name="Current Owner", value="NPC")

        if system_info["security_status"] <= 0:
            embed.color = Color.darker_grey()
        elif 0 < system_info["security_status"] < 0.49:
            embed.color = Color.dark_red()
        else:
            embed.color = Color.teal()

        await ctx.send(embed=embed)

    async def get_const_info(self, const_id, ctx):
        const_info = await self.bot.esi.constellation_info(constellation_id=const_id)
        region_info = await self.bot.esi.region_info(region_id=const_info["region_id"])

        embed = Embed(
            title=const_info["name"],
            url=f'http://evemaps.dotlan.net/map/{region_info["name"].replace(" ", "_")}/{const_info["name"].replace(" ", "_")}',
        )

        embed.add_field(name="Region", value=region_info["name"])
        embed.add_field(name="Systems", value=str(len(const_info["systems"])))

        await ctx.send(embed=embed)

    async def get_region_info(self, region_id, ctx):
        region_info = await self.bot.esi.region_info(region_id=region_id)

        embed = Embed(
            title=region_info["name"],
            url=f'http://evemaps.dotlan.net/region/{region_info["name"].replace(" ", "_")}',
        )

        embed.description = region_info["description"]

        await ctx.send(embed=embed)


def setup(bot):
    bot.add_cog(Where(bot))
