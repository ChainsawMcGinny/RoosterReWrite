import logging
import traceback
import asyncio
from nextcord.embeds import Embed
from nextcord.ext import commands

log = logging.getLogger(__name__)


class Market(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command(pass_context=True, aliases=["pc"])
    async def price(self, ctx, *, item):
        """
        Show the price info for an item, or an eft fit
        """
        if not item:
            raise commands.MissingRequiredArgument()

        await ctx.trigger_typing()
        # Big ups to Sergei Alexeyev for hosting and running this API for Fweddit.
        try:
            async with ctx.session.post(
                "https://api.gerbilsin.space/rooster/price/",
                data={"input": item},
                timeout=5,
            ) as resp:
                if resp.status != 200:
                    return await ctx.send("Price api seems to be down, try again later")
                item_info = await resp.json()
        except (TimeoutError, asyncio.TimeoutError):
            return await ctx.send(
                "The Market API seems to be down, try again later. :("
            )
        if "embedInfo" in item_info:
            if item_info["type"] == "item":
                embed = Embed(title=item_info["embedInfo"]["title"])
                try:
                    embed.url = item_info["embedInfo"]["url"]
                except KeyError:
                    pass
                try:
                    embed.set_thumbnail(url=item_info["embedInfo"]["thumbnail"]["url"])
                except KeyError:
                    pass
                for field in item_info["embedInfo"]["fields"]:
                    embed.add_field(
                        name=field["name"], value=field["value"], inline=False
                    )
                try:
                    embed.set_footer(text=item_info["embedInfo"]["footer"]["text"])
                except KeyError:
                    pass
            elif item_info["type"] == "group":
                embed = Embed(title=item_info["embedInfo"]["title"])
                try:
                    embed.set_thumbnail(url=item_info["embedInfo"]["thumbnail"]["url"])
                except:
                    pass
                for field in item_info["embedInfo"]["fields"]:
                    embed.add_field(
                        name=field["name"], value=field["value"], inline=False
                    )

            return await ctx.send(embed=embed)
        else:
            raise commands.BadArgument(item_info["error"])

    @commands.command(pass_context=True, aliases=["setprice"])
    async def set(self, ctx, *, setname):
        if not setname:
            raise commands.MissingRequiredArgument()
        try:

            async with ctx.session.post(
                "https://api.gerbilsin.space/rooster/set/",
                data={"input": setname},
                timeout=5,
            ) as resp:
                if resp.status != 200:
                    return await ctx.send("Price api seems to be down, try again later")
                set_info = await resp.json()
        except (TimeoutError, asyncio.TimeoutError):
            return await ctx.send("The Market API seems to be down, try again later :(")
        if "embedInfo" in set_info:
            e = Embed(
                title=set_info["embedInfo"]["title"],
                description=set_info["embedInfo"]["description"],
            )
            e.set_thumbnail(url=set_info["embedInfo"]["thumbnail"]["url"])
            for field in set_info["embedInfo"]["fields"]:
                e.add_field(name=field["name"], value=field["value"], inline=False)
            e.set_footer(text=set_info["embedInfo"]["footer"]["text"])

            return await ctx.send(embed=e)
        else:
            raise commands.BadArgument(set_info["error"])


def setup(bot):
    bot.add_cog(Market(bot))
