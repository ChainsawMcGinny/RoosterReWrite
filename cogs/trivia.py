import asyncio
import json
import logging
import operator
import traceback
from html.parser import HTMLParser

from nextcord.embeds import Embed
from nextcord.ext import commands
from fuzzywuzzy import fuzz

log = logging.getLogger(__name__)


class MLStripper(HTMLParser):
    """
    Simple html stripper for answers that have html code in them, really messes with answer parsing.
    """

    def __init__(self):
        super().__init__()
        self.reset()
        self.strict = False
        self.convert_charrefs = True
        self.fed = []

    def handle_data(self, d):
        self.fed.append(d)

    def get_data(self):
        return "".join(self.fed)


class Trivia(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.stripper = MLStripper()

    @commands.command(pass_context=True)
    @commands.cooldown(rate=1, per=160.0, type=commands.BucketType.channel)
    @commands.guild_only()
    async def trivia(self, ctx):
        """
        Start a round of Trivia! Only available in #trivia (access available in Auth)
        """
        if ctx.channel.name != "trivia":
            return await ctx.send("Sorry, Trivia is only allowed in #trivia!")
        for i in range(5):
            async with self.bot.session.get("http://jservice.io/api/random") as resp:
                question = await resp.json()
                question = question[0]

                if question["invalid_count"]:
                    await ctx.send(
                        "Invalid question fetched (likely an audio/visual clue.. skipping."
                    )
                else:
                    e = Embed(type="rich")
                    e.set_author(
                        name=f'Category: {question["category"]["title"]} \
                        - Difficulty/Value: ${question["value"]}'
                    )

                    e.description = question["question"]
                    self.stripper.feed(question["answer"])
                    answer = self.stripper.get_data()
                    clue = await ctx.send(embed=e)
                    await clue.add_reaction(chr(0x231B))
                    for j in reversed(range(10)):
                        # await clue.add_reaction(self.to_emoji(j))
                        try:
                            guess = await self.bot.wait_for(
                                "message",
                                timeout=3,
                                check=lambda x: fuzz.ratio(
                                    x.content.lower(), answer.lower()
                                )
                                >= 80,
                            )
                        except asyncio.TimeoutError:
                            # await clue.remove_reaction(self.to_emoji(j), ctx.me)
                            pass
                        else:
                            await clue.remove_reaction(chr(0x231B), ctx.me)
                            # await clue.remove_reaction(self.to_emoji(j), ctx.me)
                            await ctx.send(
                                f"Ding! {guess.author.name} guessed the correct answer:\n`{answer}`"
                            )
                            await self.correct_answer(ctx.guild.name, guess.author.name)
                            await clue.add_reaction(chr(0x1F44D))
                            break
                    else:
                        await clue.remove_reaction(chr(0x231B), ctx.me)
                        await ctx.send(f"No one guess the correct answer: {answer}")
                        await clue.add_reaction(chr(0x1F44E))

                self.stripper.fed = []
                await asyncio.sleep(3)
        result = await self.bot.redis.execute("get", f"trivia_{ctx.guild.name}")
        if result:
            stats = json.loads(result.decode("utf-8"))
            e = Embed(title="Top Five after this round")
            for i in sorted(stats.items(), key=operator.itemgetter(1), reverse=True)[
                :5
            ]:
                e.add_field(name=i[0], value=i[1], inline=False)
            await ctx.send(embed=e)
        else:
            await ctx.send("There are no scores on this server yet.")

        return ctx.command.reset_cooldown(ctx=ctx)

    @commands.command(pass_context=True)
    @commands.guild_only()
    async def stats(self, ctx):
        """
        Show your current stats for Trivia
        """
        result = await self.bot.redis.execute("get", f"trivia_{ctx.guild.name}")
        if result:
            scores = json.loads(result.decode("utf-8"))
            if ctx.author.name in scores:
                return await ctx.send(
                    f"{ctx.author.name} has answered {scores[ctx.author.name]} \
                questions in #trivia on this server"
                )
        await ctx.send(
            f"{ctx.author.name} has not answered any questions in #trivia on this server"
        )

    async def correct_answer(self, guild, name):
        result = await self.bot.redis.execute("get", f"trivia_{guild}")
        if result:
            scores = json.loads(result.decode("utf-8"))
            if name in scores:
                scores[name] += 1
            else:
                scores[name] = 1
        else:
            scores = {name: 1}
        await self.bot.redis.execute("set", f"trivia_{guild}", json.dumps(scores))

    def to_emoji(self, c):
        base = 0x1F550
        return chr(base + c)


def setup(bot):
    bot.add_cog(Trivia(bot))
