import datetime
import json
import logging

import dateparser
from dateparser.search import search_dates
from nextcord.ext import commands, tasks

from nextcord.embeds import Embed
from nextcord.utils import get

log = logging.getLogger(__name__)


class Reminder(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.reminder_queue.start()

    @commands.command(pass_context=True, aliases=["remindme"])
    @commands.guild_only()
    async def remind(self, ctx, *, reminder):
        """
        Create a reminder for yourself!

        Usage: !remind 12 hours brush your teeth
        """
        await ctx.trigger_typing()

        try:
            timeguess = search_dates(reminder)
        except SystemError:
            return await ctx.send(
                "I was not able to parse the time in your reminder properly"
            )
        if not timeguess:
            return await ctx.send("I was not able to parse a time in your reminder.")
        else:
            time = timeguess[0][0]
            message = reminder.replace(timeguess[0][0], "").strip()
        if not message:
            return await ctx.send(
                "I can't remind you of nothing, you already are nothing!"
            )

        if len(message) > 250:
            raise commands.BadArgument(
                "Reminder message too long, try something shorter.  Sorry"
            )

        message = self.sanitize(message)

        remindtime = dateparser.parse(
            time,
            languages=["en"],
            settings={
                "TIMEZONE": "UTC",
                "RETURN_AS_TIMEZONE_AWARE": False,
                "PREFER_DATES_FROM": "future",
            },
        )
        now = datetime.datetime.utcnow()

        if not remindtime:
            raise commands.BadArgument(
                "Whoops, I was unable to parse a time for your reminder, try something like \
            in 1 hour, 2days, 2017-12-25 00:00:00, 02:00..."
            )

        # here is the same time hack we use in the time cog for being unable to reliably parse future times
        if remindtime <= now:
            future = now - remindtime
            remindtime = remindtime + future + future

        if remindtime >= now.replace(now.year + 1):
            return await ctx.send(
                "Reminder set for longer than a year from now, perhaps try 02:00 instead of 0200"
            )

        if remindtime > now:
            result = await self.bot.redis.execute(
                "lpush",
                "reminderlist",
                json.dumps(
                    [
                        remindtime.strftime("%Y-%m-%d %H:%M:%S"),
                        ctx.message.author.id,
                        message,
                        ctx.message.channel.id,
                    ]
                ),
            )
            if result:
                return await ctx.send(
                    "Okey Dokey <@{}>, I'll remind you about that at **{}**".format(
                        ctx.message.author.id, remindtime.strftime("%Y-%m-%d %H:%M:%S")
                    )
                )
            else:
                return await ctx.send(
                    "Sorry, something went wrong and I can't remind you about that"
                )

    @commands.group(pass_context=True)
    async def reminder(self, ctx):
        """
        Utilities for active reminders
        """
        if ctx.invoked_subcommand is None:
            return await ctx.send(
                "Missing subcommand - THIS HAS CHANGED - see !help reminder for details"
            )

    @reminder.command(pass_context=True)
    @commands.guild_only()
    async def delete(self, ctx, *, number):
        """
        Delete a reminder.  To get the ID call !remind list
        """
        if not number:
            raise commands.errors.MissingRequiredArgument()

        await ctx.trigger_typing()
        result = await self.bot.redis.execute("lindex", "reminderlist", number)
        if result:
            timer, author, message, channel = json.loads(result)
            if ctx.message.author.id == author:
                await self.bot.redis.execute("lrem", "reminderlist", 1, result)
                return await ctx.send("OK.  I will no longer remind you about that.")
        else:
            return await ctx.send("That reminder ID no exist pls try again.")

    @reminder.command(pass_context=True)
    async def purge(self, ctx):
        """
        Purge all your current reminders, even for reminders not based in this channel.
        """
        await ctx.trigger_typing()
        result = await self.bot.redis.execute("llen", "reminderlist")
        if result:
            purge = []
            for item in range(result):
                r = await self.bot.redis.execute("lindex", "reminderlist", item)
                timer, author, message, channel = json.loads(r)
                if ctx.message.author.id == author:
                    purge.append(r)
            for p in purge:
                await self.bot.redis.execute("lrem", "reminderlist", 1, p)
            return await ctx.send("OK.  All your reminders have been purged!")
        else:
            return await ctx.send("There are no reminders to purge you ding dong.")

    @reminder.command(pass_context=True)
    async def list(self, ctx):
        """
        List your current reminders and get their ID, does not list 
        reminders not based in this channel for security.
        """
        await ctx.trigger_typing()
        result = await self.bot.redis.execute("llen", "reminderlist")
        if result:
            e = Embed(title=f"{ctx.message.author.name} (channel)")
            for item in range(result):
                r = await self.bot.redis.execute("lindex", "reminderlist", item)
                timer, author, message, channel = json.loads(r)
                if ctx.message.author.id == author:
                    if ctx.message.channel.id == channel:
                        e.add_field(
                            name=f"ID: {item} @ {timer}", value=message, inline=False
                        )
            return await ctx.send(embed=e)
        else:
            return await ctx.send("There are currently no reminders to list")

    @reminder.command(pass_context=True)
    async def listall(self, ctx):
        """
        List ALL your current reminders regardless of channel.  BE CAREFUL.  
        IF YOU'VE LISTED A REMINDER THAT IS OPSEC AND SHOW IT TO THE WORLD USING THIS 
        IT'S YOUR FAULT.
        """
        await ctx.trigger_typing()
        result = await self.bot.redis.execute("llen", "reminderlist")
        if result:
            e = Embed(title=f"{ctx.message.author.name} (all)")
            for item in range(result):
                r = await self.bot.redis.execute("lindex", "reminderlist", item)
                timer, author, message, channel = json.loads(r)

                if ctx.message.author.id == author:
                    e.add_field(
                        name=f"ID: {item} @ {timer}", value=message, inline=False
                    )
            return await ctx.send(embed=e)
        else:
            return await ctx.send("There are currently no reminders to list")

    @tasks.loop(seconds=15.0)
    async def reminder_queue(self):
        result = await self.bot.redis.execute("llen", "reminderlist")
        if result:
            purge = []
            for item in range(result):
                r = await self.bot.redis.execute("lindex", "reminderlist", item)
                timer, author, message, channel = json.loads(r.decode("utf-8"))
                if datetime.datetime.utcnow() > datetime.datetime.strptime(
                    timer, "%Y-%m-%d %H:%M:%S"
                ):
                    chan = get(self.bot.get_all_channels(), id=channel)
                    message = self.sanitize(message)
                    await chan.send(f"Hey, <@{author}>! {message}")
                    purge.append(r)
            for p in purge:
                await self.bot.redis.execute("lrem", "reminderlist", 1, p)

    @reminder_queue.before_loop
    async def before_reminder_queue(self):
        await self.bot.wait_until_ready()

    def sanitize(self, i):
        """
        this kills the mention abuse potential
        """
        return i.replace("@everyone", "@\u200beveryone").replace("@here", "@\u200bhere")


def setup(bot):
    bot.add_cog(Reminder(bot))
