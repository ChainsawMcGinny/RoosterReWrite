import logging
import pendulum
import traceback

import nextcord

from nextcord.ext import commands
from nextcord.embeds import Embed
from nextcord.colour import Color

log = logging.getLogger(__name__)


class About(commands.Cog):
    """
    All about me!
    """

    def __init__(self, bot):
        self.bot = bot

    @commands.command(pass_context=True)
    async def about(self, ctx):
        """
        All about the bot
        """
        await ctx.trigger_typing()

        embed = Embed(title="Rooster: The Cluckening 2 Electric Boogaloo")
        embed.set_thumbnail(
            url="https://cdn.discordapp.com/avatars/174030709051228160/fed8f758aa477dfe46a964479344e1ac.png?size=128"
        )
        embed.colour = Color.blue()

        embed.description = "Rooster is a multifunction discord bot tailored specifically for eve-online corporations."

        embed.set_footer(text="Lovingly developed for Fweddit™ by Chainsaw McGinny")

        embed.add_field(
            name="Number of Servers:", value=len(self.bot.guilds), inline=True
        )
        embed.add_field(name="Happy Customers:", value=len(self.bot.users), inline=True)
        return await ctx.send(embed=embed)

    @commands.command(hidden=True)
    async def servers(self, ctx):
        """
        Give us a list of servers we are a part of
        """
        await ctx.trigger_typing()

        embed = Embed(title="List of servers I'm on")
        for server in self.bot.guilds:
            embed.add_field(name=server.name, value=f"{len(server.members)} Members")
        return await ctx.send(embed=embed)

    @commands.command(hidden=True)
    async def uptime(self, ctx):
        """
        Returns the uptime
        """
        await ctx.send(
            pendulum.now(tz="UTC").diff_for_humans(
                self.bot.currentuptime, absolute=True
            )
        )

    @commands.command(hidden=True)
    async def invite(self, ctx):
        """Joins a server."""
        perms = discord.Permissions.none()
        perms.read_messages = True
        perms.external_emojis = True
        perms.send_messages = True
        perms.manage_roles = False
        perms.manage_channels = True
        perms.ban_members = False
        perms.kick_members = False
        perms.manage_messages = False
        perms.embed_links = True
        perms.read_message_history = True
        perms.attach_files = True
        perms.add_reactions = True
        await ctx.send(f"<{discord.utils.oauth_url(self.bot.client_id, perms)}>")


def setup(bot):
    bot.add_cog(About(bot))
