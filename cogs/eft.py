# THIS COG WOULD NOT BE POSSIBLE WITHOUT ME STEALING HALF OF PYFAS CODEBASE
# SERIOUSLY THEY DID THE HARD WORK I JUST DISCORDED IT HAVE MERCY
# https://github.com/pyfa-org/Pyfa
import logging
import aiohttp
from datetime import timedelta
import re

from cogs.utils.eft import *

from nextcord.ext import commands
from nextcord.embeds import Embed

log = logging.getLogger(__name__)


class EFT(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.eftregex = re.compile("\[.+?, .+]")

    @commands.Cog.listener()
    async def on_message(self, message):
        # get any bits of message that are in code blocks
        potentialfit = message.content.partition("```")[2].partition("```"[0])[0]
        if not potentialfit:
            return

        # chunk said message bit through pyfa importer
        fit = getfit(potentialfit)

        # if pyfa says no (like not a fit, or malformed) just bail
        if not fit:
            return

        await message.channel.trigger_typing()
        # do some calculatin on the ship with an all v toonie
        fit.character = s_queries.Character.getAll5()
        # omni damage pattern needed for tank stats like res and ehp
        fit.damagePattern = DamagePattern()
        # calculate the fit once before cranking up the dps
        fit.calculateModifiedAttributes()

        # get launchers and turrets
        turrets = []
        launchers = []
        for mod in fit.modules:
            if mod.hardpoint == FittingHardpoint.TURRET:
                turrets.append(mod)
            elif mod.hardpoint == FittingHardpoint.MISSILE:
                launchers.append(mod)

        # try to load the best ammo we got if none already loaded
        if fit.getWeaponDps().total <= 0 and fit.cargo:
            # iterate through each ammo type in the hold, if its valid see what a single guns dps is
            if turrets:
                dps = {}
                for item in fit.cargo:
                    if turrets[0].isValidCharge(item.item):
                        # modules have a charge setter method, but i cannot for the life of me get it to work
                        # so we have to brute force each charge for some reason
                        turrets[0].chargeID = item.item.ID
                        turrets[0].init()
                        turrets[0].state = FittingModuleState.ACTIVE
                        dps[item.item] = (
                            turrets[0].getDps(spoolOptions=defaultSpool).total
                        )
                if dps:
                    winner = max(dps, key=lambda k: dps[k])
                    # once we have a candidate for best (guess) dps load all the turrets with this ammo.
                    # we have to check each turret for validity in charge because I KNOW SOMEONE is gonna
                    # spam rooster with shitfits of multiple turret types and i aint about that life
                    for turret in turrets:
                        if turret.isValidCharge(winner):
                            turret.chargeID = winner.ID
                            turret.init()
                            turret.state = FittingModuleState.ACTIVE
            # repeat the process for launchers
            if launchers:
                dps = {}
                for item in fit.cargo:
                    if launchers[0].isValidCharge(item.item):
                        launchers[0].chargeID = item.item.ID
                        launchers[0].init()
                        launchers[0].state = FittingModuleState.ACTIVE
                        dps[item.item] = (
                            launchers[0].getDps(spoolOptions=defaultSpool).total
                        )
                if dps:
                    winner = max(dps, key=lambda k: dps[k])
                    for launcher in launchers:
                        if launcher.isValidCharge(winner):
                            launcher.chargeID = winner.ID
                            launcher.init()
                            launcher.state = FittingModuleState.ACTIVE

        # iterate through the drone bay to find the highest dps drone
        if fit.drones:
            dps = {}
            for d in fit.drones:
                d.amountActive = 1
                dps[d] = d.getDps().total
                d.amountActive = 0
            winner = max(dps, key=lambda k: dps[k])
            dronebay = winner.amount
            candeploy = fit.getReleaseLimitForDrone(winner.item)
            # using smaller drones can result in more being allowed than are allowed
            if candeploy > fit.extraAttributes["maxActiveDrones"]:
                candeploy = int(fit.extraAttributes["maxActiveDrones"])
            if dronebay <= candeploy:
                winner.amountActive = dronebay
            else:
                winner.amountActive = candeploy
            droneused = (
                str(winner.amountActive) + "x " + queries.getItem(winner.item.ID).name
            )

        # recalculate the fit calculate to get our weapon/drone damages now that we have turned it all on
        fit.calculateModifiedAttributes()
        dpsstring = ""
        if fit.getWeaponDps().total > 0:
            dpsstring += f"**Weapon DPS / Volley: ** {fit.getWeaponDps(spoolOptions=defaultSpool).total:,.2f} / {fit.getWeaponVolley(spoolOptions=defaultSpool).total:,.2f}\n"
            # we are just gonna assume each turret and each launcher is loaded with the same type ammo
            if turrets:
                if turrets[0].charge:
                    turretammo = turrets[0].charge.name
                    turretopt = turrets[0].maxRange
                    turretfall = turrets[0].falloff
                    dpsstring += f"using {turretammo} @ {turretopt/1000:,.2f}km+{turretfall/1000:,.2f}km\n"
            if launchers:
                if launchers[0].charge:
                    launcherammo = launchers[0].charge.name
                    launcherrange = launchers[0].maxRange
                    dpsstring += f"{launcherammo} @ {launcherrange/1000:,.2f}km\n"
        if fit.getDroneDps().total > 0:
            dpsstring += f"**Drone DPS / Volley: **: {fit.getDroneDps().total:,.2f} / {fit.getDroneVolley().total:,.2f}\n"
            dpsstring += f"using {droneused}\n"

        if dpsstring:
            dpsstring += f"**Total DPS / Volley: ** {fit.getTotalDps(spoolOptions=defaultSpool).total:,.2f} / {fit.getTotalVolley(spoolOptions=defaultSpool).total:,.2f}\n"

        # price check
        # im not a big fan of making a new session here, but i dont see an easy way to get ahold of our normal method
        async with aiohttp.ClientSession() as session:
            async with session.post(
                "https://evepraisal.com/appraisal",
                data={"market": "jita", "raw_textarea": potentialfit},
            ) as resp:
                if resp.status != 200:
                    price = -1
                else:
                    app_id = resp.headers["X-Appraisal-Id"]
                    app_url = f"https://evepraisal.com/a/{app_id}.json"
            async with session.get(app_url) as resp:
                if resp.status != 200:
                    price = -1
                else:
                    price = await resp.json()
                    price = price["totals"]["sell"]

        embed = Embed(title=f"{fit.ship.name} - {fit.name}")
        embed.set_thumbnail(
            url=f"https://image.eveonline.com/Render/{fit.shipID}_128.png"
        )
        embed.add_field(name="EHP (Omni)", value=f"{round(sum(fit.ehp.values())):,}")

        if fit.capStable:
            embed.add_field(name="Capacitor", value=f"Stable @ {round(fit.capState)}%")
        else:
            embed.add_field(
                name="Cap length",
                value=f"{str(timedelta(seconds=round(fit.capState)))}",
            )
        embed.add_field(name="Max Speed:", value=f"{fit.maxSpeed:,.2f} m/s")
        # turn all our mods off to calculate our 'actual' align time
        for module in fit.modules:
            module.state = FittingModuleState.ONLINE
        fit.calculateModifiedAttributes()
        embed.add_field(name="Align time", value=f"{fit.alignTime:.2f} Seconds")

        embed.add_field(name="Total Price", value=f"{price:,.2f} isk")
        if dpsstring:
            embed.add_field(name="Damage", value=dpsstring)

        # active reps, also includes normal shield regen, so maybe check for that before using this
        # embed.add_field(name="Active Tank", value=f"{sum(fit.effectiveSustainableTank.values()):.2f} EHP/s")

        # i really really want to include the resistance profiles, but its a lot of info and will be spammy
        # for tankType in ("shield", "armor", "hull"):
        #    for damageType in ("em", "thermal", "kinetic", "explosive"):
        #        if fit is not None:
        #            resonanceType = tankType if tankType != "hull" else ""
        #            resonance = "%s%sDamageResonance" % (resonanceType, damageType.capitalize())
        #            resonance = resonance[0].lower() + resonance[1:]
        #            resonance = (1 - fit.ship.getModifiedItemAttr(resonance)) * 100
        #        else:
        #            resonance = 0
        #        embed.add_field(name=tankType, value=f"{damageType.capitalize()} {resonance.__round__(2)}")
        return await message.channel.send(embed=embed)


def setup(bot):
    bot.add_cog(EFT(bot))
