import asyncio
import logging

import dateparser
import pendulum
import traceback
from nextcord.ext import commands

from nextcord import embeds

log = logging.getLogger(__name__)


class Time(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.group(pass_context=True)
    async def time(self, ctx):
        """
        Get current eve time - !help time for more options
        """
        if ctx.invoked_subcommand is None:
            await ctx.trigger_typing()
            e = embeds.Embed(title="Current Time")
            dt = pendulum.now(tz="UTC")
            e.add_field(name="Eve Time", value=pendulum.now(tz='UTC').format("MMMM Do, YYYY HH:mm A"))
            e.add_field(name="Local Time", value=f"<t:{dt.int_timestamp}>")
            return await ctx.send(
                embed=e
                )

    @time.command()
    async def until(self, ctx, *, time):
        """
        Attempts to tell you how long until <time> in EVE time.
        """
        await ctx.trigger_typing()

        parsedtime = dateparser.parse(
            time, settings={"TIMEZONE": "UTC", "PREFER_DATES_FROM": "future"}
        )
        if not parsedtime:
            raise commands.BadArgument("Sorry, I cannot process that time argument")
        parsedtime = pendulum.instance(parsedtime, tz="UTC")
        now = pendulum.now(tz="UTC")

        # utcnow = datetime.datetime.utcnow()

        # dateparser can struggle with some human terms like 3 hours vs in 3 hours, so we force literally everything
        # to be parsed as the future.  it's a dum hack and I'd like to improve this
        if parsedtime < now:
            future = now - parsedtime
            parsedtime = now + future + future

        timediff = embeds.Embed()

        timediff.add_field(
            name="Current EVE Time:", value=now.format('MMMM DD, YYYY HH:mm'), inline=False
        )
        pt = parsedtime.int_timestamp
        timediff.add_field(
            name="Interpreted Time:",
            value=parsedtime.format('MMMM DD, YYYY HH:mm'),
            #value=f'<t:{pt}>',
            inline=False,
        )
        timediff.add_field(
            name="Local Time of Event:",
            value=f'<t:{pt}>',
            inline=False,
        )
        timediff.add_field(
            name="Time Until:", value=(parsedtime - now).in_words(), inline=False
        )

        return await ctx.send(embed=timediff)

    @time.command()
    async def add(self, ctx, *, time):
        """
        Adds <time> to the current eve time
        """
        await ctx.trigger_typing()
        parsedtime = dateparser.parse(
            time, settings={"TIMEZONE": "UTC", "PREFER_DATES_FROM": "future"}
        )
        if not parsedtime:
            raise commands.BadArgument("Sorry, I cannot process that time argument")
        parsedtime = pendulum.instance(parsedtime, tz="UTC")

        now = pendulum.now(tz="UTC")
        newtime = now + now.diff(parsedtime)

        timeadd = embeds.Embed()
        timeadd.add_field(
            name="Current EVE Time:",
            value=pendulum.now(tz="UTC").to_datetime_string(),
            inline=False,
        )
        timeadd.add_field(
            name="New EVE Time:", value=newtime.to_datetime_string(), inline=False
        )

        return await ctx.send(embed=timeadd)

    @time.command()
    async def at(self, ctx, *, time):
        """
        gives local time using utc time, for eve conversioning
        """
        await ctx.trigger_typing()
        parsedtime = dateparser.parse(
            time, settings={"TIMEZONE": "UTC", "PREFER_DATES_FROM": "future"}
        )
        if not parsedtime:
            raise commands.BadArgument("Sorry, I cannot process that time argument")
        parsedtime = pendulum.instance(parsedtime, tz="UTC")

        timeat = embeds.Embed()
        timeat.add_field(
            name="Interpreted Time (UTC):",
            value=parsedtime.format('MMMM DD, YYYY HH:mm'),
            # value=f'<t:{pt}>',
            inline=False,
        )
        pt = parsedtime.int_timestamp
        timeat.add_field(
            name="Local Time",
            value=f'<t:{pt}>',
            inline=False,
        )
        return await ctx.send(embed=timeat)

def setup(bot):
    bot.add_cog(Time(bot))
