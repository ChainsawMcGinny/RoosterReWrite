import logging
import traceback

from nextcord.embeds import Embed
from nextcord.ext import commands
from nextcord.utils import get


log = logging.getLogger(__name__)


class JoinChannel(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    @commands.guild_only()
    async def channels(self, ctx):
        """
        List available opt-in channels on this server in category 'optional'
        """
        category = get(ctx.guild.categories, name="optional")
        if not category:
            return await ctx.send(
                f"The server {ctx.guild.name} does not have an 'optional' text"
                f" channel category to join to."
            )
        if not category.channels:
            return await ctx.send(
                f"There are no opt-in channels on {ctx.guild.name} to join"
            )
        else:
            embed = Embed(
                title=f"Current opt-in channels on {ctx.guild.name} - !join <channelname> in any room"
            )
            for chan in category.channels:
                if not chan.topic:  # how does this happen
                    chan.topic = None
                embed.add_field(name=chan.name, value=chan.topic, inline=False)
            return await ctx.author.send(embed=embed)

    @commands.command()
    @commands.guild_only()
    @commands.bot_has_permissions(manage_channels=True)
    async def join(self, ctx, *, channel):
        """
        Join an opt-in channel
        """
        if not channel:
            raise commands.MissingRequiredArgument()

        category = get(ctx.guild.categories, name="optional")
        if not category:
            return await ctx.send("Sorry, this server has no opt-in channels")
        joinchan = get(category.channels, name=channel)
        if not category.channels or not joinchan:
            return await ctx.send(f"Sorry, this server does not have a {channel} channel")

        await joinchan.set_permissions(
            ctx.message.author, read_messages=True, send_messages=True, embed_links=True
        )
        return await ctx.send(f"Gates open, welcome to the {channel} channel!")

    @commands.command(pass_context=True, aliases=["yeet"])
    @commands.guild_only()
    @commands.bot_has_permissions(manage_channels=True)
    async def leave(self, ctx, *, channel):
        """
        Leave an opt-in channel
        """
        if not channel:
            raise commands.MissingRequiredArgument()

        category = get(ctx.guild.categories, name="optional")
        if not category:
            return await ctx.send("You cannot leave a channel not in the opt-in category.")
        leavechan = get(category.channels, name=channel)
        if not category or not category.channels or not leavechan:
            return await ctx.send("Either that channel doesn't exist, or you cannot leave it.")

        overwrites = leavechan.overwrites_for(ctx.message.author)
        if not overwrites:
            return await ctx.send("You cannot leave a channel you are not already in.")

        await leavechan.set_permissions(ctx.message.author, overwrite=None)
        return await ctx.send(f"You have left {channel}.")


def setup(bot):
    bot.add_cog(JoinChannel(bot))
