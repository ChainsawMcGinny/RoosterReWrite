from enum import IntEnum
import logging
import re
from eos.const import FittingSlot
from eos.utils.spoolSupport import SpoolOptions
from eos.const import SpoolType
import eos
import eos.db
from eos.gamedata import *
from eos.db.gamedata.queries import getDynamicItem
from eos.saveddata.booster import Booster
from eos.saveddata.implant import Implant
from eos.saveddata.cargo import Cargo
from eos.saveddata.fighter import Fighter
from eos.saveddata.drone import Drone
from eos.saveddata.ship import Ship
from eos.saveddata.citadel import Citadel
from eos.saveddata.module import Module
from eos.saveddata.fit import Fit
from eos.const import FittingSlot
from eos.db.gamedata.queries import getAttributeInfo
from eos.saveddata.damagePattern import DamagePattern
from eos.const import FittingModuleState
from eos.const import FittingHardpoint
import eos.db.gamedata.queries as queries
import eos.db.saveddata.queries as s_queries


# service/const.py
class PortEftOptions(IntEnum):
    """
    Contains different options for eft-export
    """

    IMPLANTS = 1
    MUTATIONS = 2
    LOADED_CHARGES = 3
    CARGO = 4


# service/port/eft.py
EFT_OPTIONS = (
    (
        PortEftOptions.LOADED_CHARGES,
        "Loaded Charges",
        "Export charges loaded into modules",
        True,
    ),
    (
        PortEftOptions.MUTATIONS,
        "Mutated Attributes",
        "Export mutated modules' stats",
        True,
    ),
    (
        PortEftOptions.IMPLANTS,
        "Implants && Boosters",
        "Export implants and boosters",
        True,
    ),
    (PortEftOptions.CARGO, "Cargo", "Export cargo hold contents", True),
)

MODULE_CATS = ("Module", "Subsystem", "Structure Module")
SLOT_ORDER = (
    FittingSlot.LOW,
    FittingSlot.MED,
    FittingSlot.HIGH,
    FittingSlot.RIG,
    FittingSlot.SUBSYSTEM,
    FittingSlot.SERVICE,
)
OFFLINE_SUFFIX = "/OFFLINE"
NAME_CHARS = "[^,/\[\]]"

# this is the same default spooling behavior that is included in pyfa
defaultSpool = SpoolOptions(SpoolType.SPOOL_SCALE, 1.0, False)

###
# most everything past this point has been ripped straight from pyfa/eos with minor modification
# i am but a script kiddie and not very good at this
# https://github.com/pyfa-org/Pyfa
def getfit(lines):
    lines = _importPrepare(lines)
    try:
        fit = _importCreateFit(lines)
    except EftImportError:
        return

    aFit = AbstractFit()
    aFit.mutations = _importGetMutationData(lines)

    stubPattern = "^\[.+?\]$"
    modulePattern = "^(?P<typeName>{0}+?)(,\s*(?P<chargeName>{0}+?))?(?P<offline>\s*{1})?(\s*\[(?P<mutation>\d+?)\])?$".format(
        NAME_CHARS, OFFLINE_SUFFIX
    )
    droneCargoPattern = "^(?P<typeName>{}+?) x(?P<amount>\d+?)$".format(NAME_CHARS)

    sections = []
    for section in _importSectionIter(lines):
        for line in section.lines:
            # Stub line
            if re.match(stubPattern, line):
                section.itemSpecs.append(None)
                continue
            # Items with quantity specifier
            m = re.match(droneCargoPattern, line)
            if m:
                try:
                    itemSpec = MultiItemSpec(m.group("typeName"))
                # Items which cannot be fetched are considered as stubs
                except EftImportError:
                    section.itemSpecs.append(None)
                else:
                    itemSpec.amount = int(m.group("amount"))
                    section.itemSpecs.append(itemSpec)
                continue
            # All other items
            m = re.match(modulePattern, line)
            if m:
                try:
                    itemSpec = RegularItemSpec(
                        m.group("typeName"), chargeName=m.group("chargeName")
                    )
                # Items which cannot be fetched are considered as stubs
                except EftImportError:
                    section.itemSpecs.append(None)
                else:
                    if m.group("offline"):
                        itemSpec.offline = True
                    if m.group("mutation"):
                        itemSpec.mutationIdx = int(m.group("mutation"))
                    section.itemSpecs.append(itemSpec)
                continue
        _clearTail(section.itemSpecs)
        sections.append(section)

    hasDroneBay = any(s.isDroneBay for s in sections)
    hasFighterBay = any(s.isFighterBay for s in sections)
    for section in sections:
        if section.isModuleRack:
            aFit.addModules(section.itemSpecs)
        elif section.isImplantRack:
            for itemSpec in section.itemSpecs:
                aFit.addImplant(itemSpec)
        elif section.isDroneBay:
            for itemSpec in section.itemSpecs:
                aFit.addDrone(itemSpec)
        elif section.isFighterBay:
            for itemSpec in section.itemSpecs:
                aFit.addFighter(itemSpec)
        elif section.isCargoHold:
            for itemSpec in section.itemSpecs:
                aFit.addCargo(itemSpec)
        # Mix between different kinds of item specs (can happen when some
        # blank lines are removed)
        else:
            for itemSpec in section.itemSpecs:
                if itemSpec is None:
                    continue
                if itemSpec.isModule:
                    aFit.addModule(itemSpec)
                elif itemSpec.isImplant:
                    aFit.addImplant(itemSpec)
                elif itemSpec.isDrone and not hasDroneBay:
                    aFit.addDrone(itemSpec)
                elif itemSpec.isFighter and not hasFighterBay:
                    aFit.addFighter(itemSpec)
                elif itemSpec.isCargo:
                    aFit.addCargo(itemSpec)

    # Subsystems first because they modify slot amount
    for i, m in enumerate(aFit.subsystems):
        if m is None:
            dummy = Module.buildEmpty(aFit.getSlotByContainer(aFit.subsystems))
            dummy.owner = fit
            fit.modules.replaceRackPosition(i, dummy)
        elif m.fits(fit):
            m.owner = fit
            fit.modules.replaceRackPosition(i, m)
    # sneak this in here now to calculate the hull changes with subsystems
    fit.calculateModifiedAttributes()
    # Other stuff
    for modRack in (
        aFit.rigs,
        aFit.services,
        aFit.modulesHigh,
        aFit.modulesMed,
        aFit.modulesLow,
    ):
        for i, m in enumerate(modRack):
            if m is None:
                dummy = Module.buildEmpty(aFit.getSlotByContainer(modRack))
                dummy.owner = fit
                fit.modules.replaceRackPosition(i, dummy)
            elif m.fits(fit):
                m.owner = fit
                if not m.isValidState(m.state):
                    print(
                        "service.port.eft.importEft: module {} cannot have state {}",
                        m,
                        m.state,
                    )
                fit.modules.replaceRackPosition(i, m)
    for implant in aFit.implants:
        fit.implants.append(implant)
    for booster in aFit.boosters:
        fit.boosters.append(booster)
    for drone in aFit.drones.values():
        fit.drones.append(drone)
    for fighter in aFit.fighters:
        fit.fighters.append(fighter)
    for cargo in aFit.cargo.values():
        fit.cargo.append(cargo)
    return fit


class EftImportError(Exception):
    """Exception class emitted and consumed by EFT importer internally."""

    ...


def _importPrepare(lines):
    lines = lines.splitlines()
    for i in range(len(lines)):
        lines[i] = lines[i].strip()
    while lines and not lines[0]:
        del lines[0]
    while lines and not lines[-1]:
        del lines[-1]
    return lines


def _importSectionIter(lines):
    section = Section()
    for line in lines:
        if not line:
            if section.lines:
                yield section
                section = Section()
        else:
            section.lines.append(line)
    if section.lines:
        yield section


def _clearTail(lst):
    while lst and lst[-1] is None:
        del lst[-1]


def _importCreateFit(lines):
    """Create fit and set top-level entity (ship or citadel)."""
    fit = Fit()
    header = lines.pop(0)
    m = re.match("\[(?P<shipType>[\w\s]+),\s*(?P<fitName>.+)\]", header)
    if not m:
        logging.warning("service.port.eft.importEft: corrupted fit header")
        raise EftImportError
    shipType = m.group("shipType").strip()
    fitName = m.group("fitName").strip()
    try:
        ship = queries.getItem(shipType)
        try:
            fit.ship = Ship(ship)
        except ValueError:
            fit.ship = Citadel(ship)
        fit.name = fitName
    except:
        logging.warning(
            "service.port.eft.importEft: exception caught when parsing header"
        )
        raise EftImportError
    return fit


def activeStateLimit(itemIdentity):
    item = eos.db.getItem(itemIdentity.ID)
    if {
        "moduleBonusAssaultDamageControl",
        "moduleBonusIndustrialInvulnerability",
        "microJumpDrive",
        "microJumpPortalDrive",
    }.intersection(item.effects):
        return FittingModuleState.ONLINE
    return FittingModuleState.ACTIVE


def parseMutant(lines):
    # Fetch base item type
    try:
        baseItemName = lines[0]
    except IndexError:
        return None
    baseItem = queries.getItem(baseItemName.strip())
    if baseItem is None:
        return None, None, {}
    # Fetch mutaplasmid item type and actual item
    try:
        mutaplasmidName = lines[1]
    except IndexError:
        return baseItem, None, {}
    mutaplasmidItem = queries.getItem(mutaplasmidName.strip())
    if mutaplasmidItem is None:
        return baseItem, None, {}
    mutaplasmidItem = getDynamicItem(mutaplasmidItem.ID)
    # Process mutated attribute values
    try:
        mutationsLine = lines[2]
    except IndexError:
        return baseItem, mutaplasmidItem, {}
    mutations = {}
    pairs = [p.strip() for p in mutationsLine.split(",")]
    for pair in pairs:
        try:
            attrName, value = pair.split(" ")
        except ValueError:
            continue
        try:
            value = float(value)
        except (ValueError, TypeError):
            continue
        attrInfo = getAttributeInfo(attrName.strip())
        if attrInfo is None:
            continue
        mutations[attrInfo.ID] = value
    return baseItem, mutaplasmidItem, mutations


mutantHeaderPattern = re.compile("^\[(?P<ref>\d+)\](?P<tail>.*)")


def _importGetMutationData(lines):
    data = {}
    # Format: {ref: [lines]}
    mutaLinesMap = {}
    currentMutaRef = None
    currentMutaLines = []
    consumedIndices = set()

    def completeMutaLines():
        if currentMutaRef is not None and currentMutaLines:
            mutaLinesMap[currentMutaRef] = currentMutaLines

    for i, line in enumerate(lines):
        m = mutantHeaderPattern.match(line)
        # Start and reset at header line
        if m:
            completeMutaLines()
            currentMutaRef = int(m.group("ref"))
            currentMutaLines = []
            currentMutaLines.append(m.group("tail"))
            consumedIndices.add(i)
        # Reset at blank line
        elif not line:
            completeMutaLines()
            currentMutaRef = None
            currentMutaLines = []
        elif currentMutaRef is not None:
            currentMutaLines.append(line)
            consumedIndices.add(i)
    else:
        completeMutaLines()
    # Clear mutant info from source
    for i in sorted(consumedIndices, reverse=True):
        del lines[i]
    # Run parsing
    data = {}
    for ref, mutaLines in mutaLinesMap.items():
        _, mutaType, mutaAttrs = parseMutant(mutaLines)
        data[ref] = (mutaType, mutaAttrs)
    return data


class AbstractFit:
    def __init__(self):
        # Modules
        self.modulesHigh = []
        self.modulesMed = []
        self.modulesLow = []
        self.rigs = []
        self.subsystems = []
        self.services = []
        # Non-modules
        self.implants = []
        self.boosters = []
        self.drones = {}  # Format: {item: Drone}
        self.fighters = []
        self.cargo = {}  # Format: {item: Cargo}
        # Other stuff
        self.mutations = (
            {}
        )  # Format: {reference: (mutaplamid item, {attr ID: attr value})}

    @property
    def __slotContainerMap(self):
        return {
            FittingSlot.HIGH: self.modulesHigh,
            FittingSlot.MED: self.modulesMed,
            FittingSlot.LOW: self.modulesLow,
            FittingSlot.RIG: self.rigs,
            FittingSlot.SUBSYSTEM: self.subsystems,
            FittingSlot.SERVICE: self.services,
        }

    def getContainerBySlot(self, slotType):
        return self.__slotContainerMap.get(slotType)

    def getSlotByContainer(self, container):
        slotType = None
        for k, v in self.__slotContainerMap.items():
            if v is container:
                slotType = k
                break
        return slotType

    def addModules(self, itemSpecs):
        modules = []
        slotTypes = set()
        for itemSpec in itemSpecs:
            if itemSpec is None:
                modules.append(None)
                continue
            m = self.__makeModule(itemSpec)
            if m is None:
                modules.append(None)
                continue
            modules.append(m)
            slotTypes.add(m.slot)
        _clearTail(modules)
        # If all the modules have same slot type, put them to appropriate
        # container with stubs
        if len(slotTypes) == 1:
            slotType = tuple(slotTypes)[0]
            self.getContainerBySlot(slotType).extend(modules)
        # Otherwise, put just modules
        else:
            for m in modules:
                if m is None:
                    continue
                self.getContainerBySlot(m.slot).append(m)

    def addModule(self, itemSpec):
        if itemSpec is None:
            return
        m = self.__makeModule(itemSpec)
        if m is not None:
            self.getContainerBySlot(m.slot).append(m)

    def __makeModule(self, itemSpec):
        # Mutate item if needed
        m = None
        if itemSpec.mutationIdx in self.mutations:
            mutaItem, mutaAttrs = self.mutations[itemSpec.mutationIdx]
            mutaplasmid = getDynamicItem(mutaItem.ID)
            if mutaplasmid:
                try:
                    m = Module(mutaplasmid.resultingItem, itemSpec.item, mutaplasmid)
                except ValueError:
                    pass
                else:
                    for attrID, mutator in m.mutators.items():
                        if attrID in mutaAttrs:
                            mutator.value = mutaAttrs[attrID]
        # If we still don't have item (item is not mutated or we
        # failed to construct mutated item), try to make regular item
        if m is None:
            try:
                m = Module(itemSpec.item)
            except ValueError:
                return None

        if itemSpec.charge is not None and m.isValidCharge(itemSpec.charge):
            m.charge = itemSpec.charge
        if itemSpec.offline and m.isValidState(FittingModuleState.OFFLINE):
            m.state = FittingModuleState.OFFLINE
        elif m.isValidState(FittingModuleState.ACTIVE):
            m.state = activeStateLimit(m.item)
        return m

    def addImplant(self, itemSpec):
        if itemSpec is None:
            return
        if "implantness" in itemSpec.item.attributes:
            self.implants.append(Implant(itemSpec.item))
        elif "boosterness" in itemSpec.item.attributes:
            self.boosters.append(Booster(itemSpec.item))
        else:
            pyfalog.error("Failed to import implant: {}", itemSpec.typeName)

    def addDrone(self, itemSpec):
        if itemSpec is None:
            return
        if itemSpec.item not in self.drones:
            self.drones[itemSpec.item] = Drone(itemSpec.item)
        self.drones[itemSpec.item].amount += itemSpec.amount

    def addFighter(self, itemSpec):
        if itemSpec is None:
            return
        fighter = Fighter(itemSpec.item)
        fighter.amount = itemSpec.amount
        self.fighters.append(fighter)

    def addCargo(self, itemSpec):
        if itemSpec is None:
            return
        if itemSpec.item not in self.cargo:
            self.cargo[itemSpec.item] = Cargo(itemSpec.item)
        self.cargo[itemSpec.item].amount += itemSpec.amount


class Section:
    def __init__(self):
        self.lines = []
        self.itemSpecs = []
        self.__itemDataCats = None

    @property
    def itemDataCats(self):
        if self.__itemDataCats is None:
            cats = set()
            for itemSpec in self.itemSpecs:
                if itemSpec is None:
                    continue
                cats.add(itemSpec.item.category.name)
            self.__itemDataCats = tuple(sorted(cats))
        return self.__itemDataCats

    @property
    def isModuleRack(self):
        return all(i is None or i.isModule for i in self.itemSpecs)

    @property
    def isImplantRack(self):
        return all(i is not None and i.isImplant for i in self.itemSpecs)

    @property
    def isDroneBay(self):
        return all(i is not None and i.isDrone for i in self.itemSpecs)

    @property
    def isFighterBay(self):
        return all(i is not None and i.isFighter for i in self.itemSpecs)

    @property
    def isCargoHold(self):
        return (
            all(i is not None and i.isCargo for i in self.itemSpecs)
            and not self.isDroneBay
            and not self.isFighterBay
        )


class BaseItemSpec:
    def __init__(self, typeName):
        item = queries.getItem(typeName)
        if item is None:
            raise EftImportError
        self.typeName = typeName
        self.item = item

    @property
    def isModule(self):
        return False

    @property
    def isImplant(self):
        return False

    @property
    def isDrone(self):
        return False

    @property
    def isFighter(self):
        return False

    @property
    def isCargo(self):
        return False


class RegularItemSpec(BaseItemSpec):
    def __init__(self, typeName, chargeName=None):
        super().__init__(typeName)
        self.charge = self.__fetchCharge(chargeName)
        self.offline = False
        self.mutationIdx = None

    def __fetchCharge(self, chargeName):
        if chargeName:
            charge = queries.getItem(chargeName)
            if not charge or charge.category.name != "Charge":
                charge = None
        else:
            charge = None
        return charge

    @property
    def isModule(self):
        return self.item.category.name in MODULE_CATS

    @property
    def isImplant(self):
        return self.item.category.name == "Implant" and (
            "implantness" in self.item.attributes
            or "boosterness" in self.item.attributes
        )


class MultiItemSpec(BaseItemSpec):
    def __init__(self, typeName):
        super().__init__(typeName)
        self.amount = 0

    @property
    def isDrone(self):
        return self.item.category.name == "Drone"

    @property
    def isFighter(self):
        return self.item.category.name == "Fighter"

    @property
    def isCargo(self):
        return True
