import logging
from urllib.error import HTTPError
from esipysi import EsiPysi, EsiAuth
from esipysi.cache import RedisCache
import redis
import config

log = logging.getLogger(__name__)


class SwaggerError(Exception):
    pass


class ESI:
    def __init__(self):
        r = redis.StrictRedis(host=config.redis_host, port=config.redis_port, db=1)
        self.cache = RedisCache(r)
        self.esi = EsiPysi(
            "https://esi.evetech.net/_latest/swagger.json?datasource=tranquility",
            user_agent=config.email,
            cache=self.cache,
        )

        # self.auth_esi = EsiAuth(config.client_id, config.secret_key)

    async def search(self, scope, entity):
        async with self.esi.session() as sesh:
            entity_search = sesh.get_operation("get_search")
            result = await entity_search.execute(
                categories=scope, search=entity, strict="true"
            )
            return result.json()

    async def loose_search(self, scope, entity):
        async with self.esi.session() as sesh:
            entity_search = sesh.get_operation("get_search")
            result = await entity_search.execute(
                categories=scope, search=entity, strict="false"
            )
            return result.json()

    async def kill_lookup(self, kill_id, kill_hash):
        async with self.esi.session() as sesh:
            kill_call = sesh.get_operation("get_killmails_killmail_id_killmail_hash")
            result = await kill_call.execute(
                killmail_id=kill_id, killmail_hash=kill_hash
            )
            return result.json()

    async def character_sheet(self, character_id):
        async with self.esi.session() as sesh:
            character_sheet_op = sesh.get_operation("get_characters_character_id")
            result = await character_sheet_op.execute(character_id=character_id)
            return result.json()

    async def corporation_history(self, character_id):
        async with self.esi.session() as sesh:
            corp_history_op = sesh.get_operation(
                "get_characters_character_id_corporationhistory"
            )
            result = await corp_history_op.execute(character_id=character_id)
            return result.json()

    async def corporation_sheet(self, corporation_id):
        async with self.esi.session() as sesh:
            corporation_sheet_op = sesh.get_operation("get_corporations_corporation_id")
            result = await corporation_sheet_op.execute(corporation_id=corporation_id)
            return result.json()

    async def alliance_sheet(self, alliance_id):
        async with self.esi.session() as sesh:
            alliance_sheet_op = sesh.get_operation("get_alliances_alliance_id")
            result = await alliance_sheet_op.execute(alliance_id=alliance_id)
            return result.json()

    async def insurance_prices(self):
        async with self.esi.session() as sesh:
            insurance_op = sesh.get_operation("get_insurance_prices")
            result = await insurance_op.execute()
            return result.json()

    async def ship_info(self, ship_id):
        async with self.esi.session() as sesh:
            ship_op = sesh.get_operation("get_universe_types_type_id")
            result = await ship_op.execute(type_id=ship_id)
            return result.json()

    async def system_info(self, system_id):
        async with self.esi.session() as sesh:
            system_op = sesh.get_operation("get_universe_systems_system_id")
            result = await system_op.execute(system_id=system_id)
            return result.json()

    async def constellation_info(self, constellation_id):
        async with self.esi.session() as sesh:
            const_op = sesh.get_operation(
                "get_universe_constellations_constellation_id"
            )
            result = await const_op.execute(constellation_id=constellation_id)
            return result.json()

    async def region_info(self, region_id):
        async with self.esi.session() as sesh:
            region_op = sesh.get_operation("get_universe_regions_region_id")
            result = await region_op.execute(region_id=region_id)
            return result.json()

    async def route(self, origin_id, destination_id, flag):
        async with self.esi.session() as sesh:
            route_op = sesh.get_operation("get_route_origin_destination")
            try:
                result = await route_op.execute(
                    origin=origin_id, destination=destination_id, flag=flag
                )
            except HTTPError:
                raise SwaggerError(
                    "No Route Found, Did you try to navigate to a wormhole maybe?"
                )
            else:
                return result.json()

    async def universe_names(self, ids):
        async with self.esi.session() as sesh:
            name_op = sesh.get_operation("post_universe_names")
            result = await name_op.execute(ids=ids)
            return result.json()

    async def system_kills(self):
        async with self.esi.session() as sesh:
            kills_op = sesh.get_operation("get_universe_system_kills")
            result = await kills_op.execute()
            return result.json()

    async def system_jumps(self):
        async with self.esi.session() as sesh:
            jumps_op = sesh.get_operation("get_universe_system_jumps")
            result = await jumps_op.execute()
            return result.json()

    async def sov_map(self):
        async with self.esi.session() as sesh:
            sov_op = sesh.get_operation("get_sovereignty_map")
            result = await sov_op.execute()
            return result.json()

    async def system_adm(self):
        async with self.esi.session() as sesh:
            adm_op = sesh.get_operation("get_sovereignty_structures")
            result = await adm_op.execute()
            return result.json()
