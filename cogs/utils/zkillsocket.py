import asyncio
import websockets
import logging
import datetime
import time
import json
from multiprocessing import Process, Event


class zKillSocket(Process):
    def __init__(self, kill_queue):
        super(zKillSocket, self).__init__()
        self.daemon = True
        self.loop = None
        self.listening = False
        self.queue = kill_queue
        self.exit = Event()
        self.log = logging.getLogger(__name__)

    def run(self):
        try:
            self.loop = asyncio.get_event_loop()
            while not self.exit.is_set():
                try:
                    self.loop.run_until_complete(self.__connect())
                except Exception:
                    self.log.exception("An error occured in the zKill socket")
                    self.listening = False
        except Exception:
            self.log.exception("Unhandled exception occured")

    def stop(self):
        self.exit.set()

    async def __connect(self):
        print("Connecting to zKill socket...")
        async with websockets.connect("wss://zkillboard.com/websocket/") as zkillsocket:
            await zkillsocket.send('{"action":"sub","channel":"killstream"}')
            self.listening = True
            print("Connected and listening to zKill")
            while zkillsocket.open and not self.exit.is_set():
                message = await self.__get_message(zkillsocket)
                if message is None:
                    break
                kill_json = json.loads(message)
                self.log.debug("Got kill: {}".format(kill_json.get("killmail_id", "?")))
                self.queue.put(kill_json)
        self.log.info("zKill socket closed")
        self.listening = False

    async def __get_message(self, websocket):
        if not websocket.open:
            self.log.error("zKill socket is not open, restarting...")
            return None
        while websocket.open and not self.exit.is_set():
            try:
                message = await asyncio.wait_for(websocket.recv(), timeout=600)
            except asyncio.TimeoutError:
                self.log.warning("zKill has timed out, restarting...")
                return None
            except Exception:
                self.log.exception(
                    "An exception occured while waiting for a message from the zKill socket"
                )
                return None
            else:
                return message
