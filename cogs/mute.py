import datetime
import pendulum
import logging
import traceback
import asyncio

from nextcord.ext import commands
from nextcord import Member

log = logging.getLogger(__name__)


class Mute(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command(pass_context=True)
    @commands.guild_only()
    @commands.has_permissions(manage_messages=True)
    @commands.bot_has_permissions(moderate_members=True)
    async def mute(self, ctx, *, name: Member):
        """
        Mute a member in all channels, disallowing them to speak, now with less jank!
        """
        if not name:
            raise commands.MissingRequiredArgument()

        time = pendulum.now()
        time = time.add(weeks=1)
        m = await name.edit(timeout=time)
        if m:
            await ctx.send(f'User {name.display_name} has been muted.')
        else:
            await ctx.send(f'I could not mute {name.display_name}')


    @commands.command(pass_context=True)
    @commands.guild_only()
    @commands.has_permissions(manage_messages=True)
    @commands.bot_has_permissions(moderate_members=True)
    async def unmute(self, ctx, *, name: Member):
        if not name:
            raise commands.MissingRequiredArgument()

        await name.edit(timeout=None)

        await ctx.send(f'User {name.display_name} is no longer muted.')


def setup(bot):
    bot.add_cog(Mute(bot))
