import logging
from nextcord.embeds import Embed
from nextcord.ext import commands
import pendulum

log = logging.getLogger(__name__)


# Ugly but it works
def security_to_type(security: int) -> str:
    if security <= 0:
        return "nullsec"
    elif 0 < security < 0.5:
        return "lowsec"
    else:
        return "highsec"


class Thera(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.api = "https://www.eve-scout.com/api/wormholes?systemSearch={}"

    @commands.command(pass_context=True)
    async def thera(self, ctx, system, force_status=None):
        """
        Search Eve-Scout for the nearest reported thera hole near [system]
        Append <highsec|lowsec|nullsec> at the end to only search for exits in systems with such security status.
        """
        if not system:
            raise commands.MissingRequiredArgument("Missing a system name.")

        await ctx.trigger_typing()
        origin_lookup = await self.bot.esi.loose_search("solar_system", system)

        try:
            system_id = origin_lookup["solar_system"][0]
        except KeyError:
            raise commands.BadArgument("Cannot find a system with that name!")

        system_info = await self.bot.esi.system_info(system_id=system_id)
        try:
            system_name = system_info["name"]
        except KeyError:
            raise commands.BadArgument("Could not fetch system info!")

        if system_name == "Thera":
            return await ctx.send("Are you already in Thera?")

        async with self.bot.session.get(self.api.format(system_name)) as response:
            if response.status != 200:
                raise TimeoutError
            result = await response.json()
            if not result:
                raise ValueError

            for i in sorted(result, key=lambda info: info['jumps']):
                # This will ignore wormhole systems
                if i['jumps'] <= 0:
                    continue

                destination = i["destinationSolarSystem"]

                if force_status is not None:
                    if security_to_type(destination["security"]) != force_status:
                        continue

                time_until_eol = pendulum.parse(i["wormholeEstimatedEol"])

                embed = Embed(title=f"Closest Thera connection from {system_name}")
                embed.add_field(
                    name="System",
                    value=f'{destination["name"]} ({destination["security"]:.2f})'
                )
                embed.add_field(
                    name="Region",
                    value=destination["region"]["name"],
                )
                embed.add_field(name="Jumps", value=i["jumps"], inline=True)
                embed.add_field(
                    name="Out Sig", value=i["signatureId"], inline=True
                )
                embed.add_field(
                    name="In Sig",
                    value=i["wormholeDestinationSignatureId"],
                    inline=True,
                )
                embed.add_field(
                    name="Mass", value=i["wormholeMass"].capitalize()
                )
                embed.add_field(
                    name="Est. EOL",
                    value=f"Less than {time_until_eol.diff_for_humans(absolute=True)}",
                )
                embed.description = f'http://evemaps.dotlan.net/route/{system_name}:{destination["name"]}'
                embed.set_footer(text="All data is pulled from eve-scout.com")
                return await ctx.send(embed=embed)

        return await ctx.send("Could not find a connection to Thera.")


def setup(bot):
    bot.add_cog(Thera(bot))
