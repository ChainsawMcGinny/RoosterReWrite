import logging

from nextcord.ext import commands
from pint import UnitRegistry

log = logging.getLogger(__name__)


class Units(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.unitparse = UnitRegistry(autoconvert_offset_to_baseunit=True)
        self.Q = self.unitparse.Quantity

    @commands.command()
    async def convert(self, ctx, *, unitstring):
        """Convert basic units
        Usage: Convert 10 kg to lbs
        see https://github.com/hgrecco/pint/blob/master/pint/default_en.txt for more help"""
        if not unitstring:
            raise commands.MissingRequiredArgument()
        try:
            src, dst = unitstring.split(" to ")
        except ValueError:
            return await ctx.send(
                'Error unpacking your conversion, try something like "3 pounds to kilograms"'
            )

        return await ctx.send(round(self.Q(src).to(dst), 2))


def setup(bot):
    bot.add_cog(Units(bot))
