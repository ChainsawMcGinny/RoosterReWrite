import logging
import asyncio
import pendulum
from nextcord.colour import Color
from nextcord.embeds import Embed
from nextcord.ext import commands

from aiohttp import client_exceptions

log = logging.getLogger(__name__)


class Who(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.group(pass_context=True)
    async def who(self, ctx):
        """
        Public infos on Characters, Corps, and Alliances
        """
        if ctx.invoked_subcommand is None:
            # no argument will default to attempt a char lookup,
            await ctx.invoke(self.char, character=ctx.message.content[5:])

    @who.command()
    async def char(self, ctx, *, character):
        """
        Lookup a character and get public info
        """
        if not character:
            raise commands.BadArgument(
                "character is a required argument that is missing"
            )

        await ctx.trigger_typing()
        try:
            char_lookup = await self.bot.esi.search("character", character)

            # sometimes strict isnt strict enough
            if len(char_lookup["character"]) > 1:
                for id in char_lookup["character"]:
                    name = await self.character_name_by_id(id)
                    if name.casefold() == character.casefold():
                        character_id = id
                        break
                # at least we tried
                if not character_id:
                    character_id = char_lookup["character"][0]
            else:
                character_id = char_lookup["character"][0]
        except KeyError:
            raise commands.BadArgument("Character not found")

        character_sheet = await self.bot.esi.character_sheet(character_id=character_id)
        corporation_sheet = await self.bot.esi.corporation_sheet(
            corporation_id=character_sheet["corporation_id"]
        )

        embed = Embed(
            title=character_sheet["name"],
            url=f"https://zkillboard.com/character/{character_id}/",
        )
        embed.set_thumbnail(
            url=f"https://image.eveonline.com/Character/{character_id}_256.jpg"
        )

        birthday = pendulum.parse(character_sheet["birthday"])
        embed.add_field(
            name="Birthday",
            value=f"{birthday.to_formatted_date_string()}\n({birthday.diff_for_humans()})",
            inline=True,
        )

        security_status = round(float(character_sheet["security_status"]), 2)
        if security_status < -5:
            embed.color = Color.red()
        elif -5 < security_status < 0:
            embed.color = Color.light_grey()
        elif security_status > 0:
            embed.color = Color.teal()

        embed.add_field(name="Security Status", value=str(security_status), inline=True)

        if "alliance_id" in corporation_sheet:
            alliance_sheet = await self.bot.esi.alliance_sheet(
                alliance_id=corporation_sheet["alliance_id"]
            )

            embed.add_field(
                name="Alliance",
                value=f'{alliance_sheet["name"]} [{alliance_sheet["ticker"]}]',
                inline=True,
            )
        else:
            embed.add_field(name="Alliance", value="None", inline=True)

        corp_history = await self.bot.esi.corporation_history(character_id=character_id)
        time_in_corp = pendulum.parse(corp_history[0]["start_date"]).diff_for_humans()

        embed.add_field(
            name="Corporation",
            value=f'{corporation_sheet["name"]} [{corporation_sheet["ticker"]}]\n({time_in_corp})',
            inline=True,
        )

        kills, losses, active, total = await self.zkill_stats(
            ctx, "characterID", character_id
        )
        embed.add_field(
            name="Kills / Losses",
            value=f"{int(kills):,} / {int(losses):,}",
            inline=True,
        )

        activity = await self.zkill_activity(ctx, "characterID", character_id)
        embed.add_field(name="Last killboard activity", value=activity, inline=True)

        return await ctx.send(embed=embed)

    @who.command()
    async def corp(self, ctx, *, corporation):
        """
        Lookup a corporation and get public info
        """
        if not corporation:
            raise commands.BadArgument(
                "corporation is a required argument that is missing"
            )

        await ctx.trigger_typing()
        try:
            corp_lookup = await self.bot.esi.search("corporation", corporation)
            corporation_id = corp_lookup["corporation"][0]
        except KeyError:
            raise commands.BadArgument("Corporation not found")

        corporation_sheet = await self.bot.esi.corporation_sheet(
            corporation_id=corporation_id
        )

        embed = Embed(
            title=f'{corporation_sheet["name"]} [{corporation_sheet["ticker"]}]',
            url=f"https://zkillboard.com/corporation/{corporation_id}/",
        )

        embed.set_thumbnail(
            url=f"https://image.eveonline.com/Corporation/{corporation_id}_256.png"
        )

        if "date_founded" in corporation_sheet:
            create_date = pendulum.parse(corporation_sheet["date_founded"])
        else:
            create_date = pendulum.parse("May 6, 2003")
        embed.add_field(
            name="Founded",
            value=f"{create_date.to_formatted_date_string()}\n({create_date.diff_for_humans()})",
            inline=True,
        )

        founder_name = await self.character_name_by_id(corporation_sheet["creator_id"])
        embed.add_field(name="Founder", value=founder_name, inline=True)

        ceo_name = await self.character_name_by_id(corporation_sheet["ceo_id"])
        embed.add_field(name="CEO", value=ceo_name, inline=True)

        kills, losses, active, total = await self.zkill_stats(
            ctx, "corporationID", corporation_id
        )

        embed.add_field(
            name="Members / Active 7d", value=f"{total} / {active}", inline=True
        )

        if "alliance_id" in corporation_sheet:
            alliance_sheet = await self.bot.esi.alliance_sheet(
                alliance_id=corporation_sheet["alliance_id"]
            )

            embed.add_field(
                name="Alliance",
                value=f'{alliance_sheet["name"]} [{alliance_sheet["ticker"]}]',
                inline=True,
            )
        else:
            embed.add_field(name="Alliance", value="None", inline=True)

        embed.add_field(
            name="Tax Rate", value=f'{corporation_sheet["tax_rate"]:.0%}', inline=True
        )

        embed.add_field(
            name="Kills / Losses", value=f"{kills:,} / {losses:,}", inline=True
        )

        activity = await self.zkill_activity(ctx, "corporationID", corporation_id)
        embed.add_field(name="Last killboard activity", value=activity, inline=True)

        return await ctx.send(embed=embed)

    @who.command()
    async def alliance(self, ctx, *, alliance):
        """
        Lookup an alliance and get public info
        """
        if not alliance:
            raise commands.MissingRequiredArgument

        await ctx.trigger_typing()
        try:
            alliance_lookup = await self.bot.esi.search("alliance", alliance)
            alliance_id = alliance_lookup["alliance"][0]
        except KeyError:
            raise commands.BadArgument("Alliance not found.")

        alliance_sheet = await self.bot.esi.alliance_sheet(alliance_id=alliance_id)

        embed = Embed(
            title=f'{alliance_sheet["name"]} [{alliance_sheet["ticker"]}]',
            url=f"https://zkillboard.com/alliance/{alliance_id}/",
        )
        embed.set_thumbnail(
            url=f"https://image.eveonline.com/Alliance/{alliance_id}_128.png"
        )

        founded = pendulum.parse(alliance_sheet["date_founded"])
        embed.add_field(
            name="Founded",
            value=f"{founded.to_formatted_date_string()}\n({founded.diff_for_humans()})",
            inline=True,
        )

        exec_corp_sheet = await self.bot.esi.corporation_sheet(
            corporation_id=alliance_sheet["executor_corporation_id"]
        )
        executor_name = await self.character_name_by_id(exec_corp_sheet["ceo_id"])

        embed.add_field(name="Executor", value=executor_name)

        kills, losses, active, total = await self.zkill_stats(
            ctx, "allianceID", alliance_id
        )

        embed.add_field(
            name="Members / Active 7d", value=f"{total} / {active}", inline=True
        )

        embed.add_field(
            name="Kills / Losses", value=f"{kills:,} / {losses:,}", inline=True
        )

        activity = await self.zkill_activity(ctx, "allianceID", alliance_id)
        embed.add_field(name="Last killboard activity", value=activity, inline=True)

        return await ctx.send(embed=embed)

    async def character_name_by_id(self, character_id):
        character_sheet = await self.bot.esi.character_sheet(character_id=character_id)
        if "name" in character_sheet:
            return character_sheet["name"]
        else:
            return "_unknown_"

    async def zkill_activity(self, ctx, scope, entity_id):
        # go nighty nite for a second to avoid zkill throttle
        await asyncio.sleep(1)
        async with ctx.session.get(
            f"https://zkillboard.com/api/{scope}/{entity_id}/"
        ) as resp:
            if resp.status != 200:
                return "zkill api error"
            kill_log = await resp.json()
            if not kill_log:
                last_active = "No killboard activity"
            else:
                last_killmail = await self.bot.esi.kill_lookup(
                    kill_log[0].get("killmail_id", ""),
                    kill_log[0].get("zkb", []).get("hash", ""),
                )
                last_active = pendulum.parse(
                    last_killmail.get("killmail_time", "2004-01-01")
                ).diff_for_humans()

        return last_active

    async def zkill_stats(self, ctx, scope, entity_id):
        # go nighty night for a second to avoid a zkill throttle
        await asyncio.sleep(1)
        async with ctx.session.get(
            f"https://zkillboard.com/api/stats/{scope}/{entity_id}/"
        ) as resp:
            if resp.status != 200:
                return -1, -1, -1, -1
            try:
                stats = await resp.json()
            except client_exceptions.ContentTypeError:
                return -1, -1, -1, -1
            if stats:
                kills = stats.get("shipsDestroyed", 0)
                losses = stats.get("shipsLost", 0)
            else:
                kills, losses = 0, 0

            if scope != "characterID":
                if stats["activepvp"] and "characters" in stats["activepvp"]:

                    active = stats["activepvp"]["characters"].get("count", 0)
                else:
                    active = 0
                if stats["info"]:
                    total = stats["info"].get("memberCount", 0)
                else:
                    total = 0
            else:
                active, total = None, None

        return kills, losses, active, total

    # Since all the who commands are subcommands, their aliases are already registered, so we
    # need a weird hack to alias them ourselves to their respective !who.
    @commands.command(pass_context=True, aliases=["corp"])
    async def corp_alias(self, ctx, *, corp):
        """
        Special alias to allow !corp instead of !who corp
        """
        await ctx.invoke(self.corp, corporation=corp)

    @commands.command(pass_context=True, aliases=["alliance"])
    async def alliance_alias(self, ctx, *, alliance):
        """
        Special alias to allow !alliance instead of !who alliance
        """
        await ctx.invoke(self.alliance, alliance=alliance)


def setup(bot):
    bot.add_cog(Who(bot))
