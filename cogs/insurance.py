import logging
import traceback

from nextcord.ext import commands
from nextcord.embeds import Embed

log = logging.getLogger(__name__)


class Insurance(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command(pass_context=True, aliases=["insurance"])
    async def insure(self, ctx, *, ship):
        """
        Show insurance rates for a ship.
        """
        if not ship:
            raise commands.MissingRequiredArgument(ship)

        await ctx.trigger_typing()

        ship_lookup = await self.bot.esi.search("inventory_type", ship)
        try:
            ship_id = ship_lookup["inventory_type"][0]
        except KeyError:
            raise commands.BadArgument("Ship not found")

        insurance_prices = await self.bot.esi.insurance_prices()

        for shiprates in insurance_prices:
            if shiprates["type_id"] == ship_id:
                ship_info = await self.bot.esi.ship_info(ship_id=ship_id)
                embed = Embed(title=f'Insurance Rates for {ship_info["name"]}')
                embed.set_thumbnail(
                    url=f"https://imageserver.eveonline.com/Render/{ship_id}_128.png"
                )

                for rate in shiprates["levels"]:
                    if rate["name"] == "Basic":
                        embed.add_field(
                            name="No Insurance:",
                            value=f"Payout: {(rate['payout']- (rate['payout'] * .1)) - rate['cost']:,.2f}",
                            inline=False,
                        )
                    if rate["name"] == "Platinum":
                        embed.add_field(
                            name=rate["name"],
                            value=f"_Cost_: {rate['cost']:,.2f}\n_Payout_: {rate['payout']:,.2f}\n_NET_: {rate['payout']-rate['cost']:,.2f}",
                            inline=False,
                        )
                return await ctx.send(embed=embed)

        return await ctx.send(
            "Ship not found or insurance rates not available for shipname"
        )


def setup(bot):
    bot.add_cog(Insurance(bot))
