import logging

from nextcord.embeds import Embed
from nextcord.ext import commands

log = logging.getLogger(__name__)


class EsiPrice(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command(pass_context=True)
    # TODO: add location for input to search hubs? stations?
    # TODO: better item search and naming
    # TODO clean up the embed
    # TODO history (delta) possible like the fweddit.space api?
    # TODO: better strict vs loose search attempting (we dont currently)
    async def price(self, ctx, *, item):
        if not item:
            return commands.MissingRequiredArgument(item)

        await ctx.trigger_typing()

        try:
            item_lookup = await self.bot.esi.search("inventory_type", item)

            if len(item_lookup["inventory_type"]) > 1:
                for id in item_lookup["inventory_type"]:
                    name = await self.bot.esi.universe_names(ids=id)
                    if name.casefold() == item.casefold():
                        item_id = id
                        break
                if not item_id:
                    item_id = item_lookup["iventory_type"][0]
            else:
                item_id = item_lookup["inventory_type"][0]
        except KeyError:
            raise commands.BadArgument("Item was not found")

        data = await self.market_data(ctx, item_id, 60003760)
        buymax = "{0:,.2f}".format(float(data["buy"]["max"]))
        buymin = "{0:,.2f}".format(float(data["buy"]["min"]))
        buyavg = "{0:,.2f}".format(float(data["buy"]["weightedAverage"]))
        buy_volume = "{0:,.0f}".format(float(data["buy"]["volume"]))
        buy_orders = "{0:,.0f}".format(float(data["buy"]["orderCount"]))
        sellmax = "{0:,.2f}".format(float(data["sell"]["max"]))
        sellmin = "{0:,.2f}".format(float(data["sell"]["min"]))
        sellavg = "{0:,.2f}".format(float(data["sell"]["weightedAverage"]))
        sell_volume = "{0:,.0f}".format(float(data["sell"]["volume"]))
        sell_orders = "{0:,.0f}".format(float(data["sell"]["orderCount"]))
        embed = Embed(
            title=f"Price Lookup for {item}",
            url=f"https://market.fuzzwork.co.uk/type/{item_id}/",
        )
        embed.set_thumbnail(url=f"https://image.eveonline.com/Type/{item_id}_64.png")
        embed.set_footer(text="This pricing tool is WIP so buyer beware")
        embed.add_field(
            name="Buy",
            value=(
                f"Low: {buymin}\n"
                f"Avg: {buyavg}\n"
                f"High: {buymax}\n"
                f"Number of Orders: {buy_orders}\n"
                f"Volume: {buy_volume}"
            ),
            inline=True,
        )
        embed.add_field(
            name="Sell",
            value=(
                f"Low: {sellmin}\n"
                f"Avg: {sellavg}\n"
                f"High: {sellmax}\n"
                f"Number of Orders: {sell_orders}\n"
                f"Volume: {sell_volume}"
            ),
            inline=True,
        )
        return await ctx.send(embed=embed)

    async def market_data(self, ctx, itemid, station):
        async with ctx.session.get(
            f"https://market.fuzzwork.co.uk/aggregates/?station={station}&types={itemid}"
        ) as resp:
            if resp.status != 200:
                return "Market api error"
            market_info = await resp.json()
            if not market_info:
                market_info = "No Market Data"
            return market_info[str(itemid)]


def setup(bot):
    bot.add_cog(EsiPrice(bot))
