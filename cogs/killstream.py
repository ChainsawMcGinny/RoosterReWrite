import json
import hashlib
import logging
import re
from nextcord.errors import Forbidden
import pendulum
from nextcord.ext import tasks, commands
from nextcord.colour import Color
from nextcord.embeds import Embed
from nextcord.utils import get

log = logging.getLogger(__name__)


class Killstream(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.killstream.start()

    @tasks.loop(seconds=5.0)
    async def killstream(self):
        while not self.bot.kill_queue.empty():
            kill = self.bot.kill_queue.get_nowait()
            if pendulum.parse(kill["killmail_time"]) < pendulum.now(tz="UTC").subtract(
                hours=2
            ):
                continue
            try:
                if kill["zkb"]["totalValue"] > 0:
                    pass
            except KeyError:
                continue
            result = await self.bot.redis.execute("get", "killwatch_criteria")
            if result:
                criteria = json.loads(result.decode("utf-8"))
                if criteria:
                    for item in list(criteria):
                        if criteria[item]["scope"] == "Big Kill":
                            if kill["zkb"]["totalValue"] > criteria[item]["value"]:
                                await self.announce_kill(
                                    "Big Kill", kill, criteria[item]["channel"]
                                )
                                continue
                        if criteria[item]["id"] in kill["victim"].values():
                            if kill["zkb"]["totalValue"] > criteria[item]["value"]:
                                await self.announce_kill(
                                    "Victim", kill, criteria[item]["channel"]
                                )
                            continue
                        for attacker in kill["attackers"]:
                            if criteria[item]["id"] in attacker.values():
                                if kill["zkb"]["totalValue"] > criteria[item]["value"]:
                                    await self.announce_kill(
                                        "Kill", kill, criteria[item]["channel"]
                                    )
                                    break

    async def announce_kill(self, losstype, killpackage, channel):
        kill_id = killpackage.get("killmail_id")
        # Check if kill has been announced
        kill_key = self.__kill_key(kill_id, channel)
        exists = await self.bot.redis.execute("exists", kill_key)
        if exists:
            return
        # Store
        await self.bot.redis.execute("set", kill_key, 1)
        await self.bot.redis.execute(
            "expire", kill_key, 60 * 30
        )  # Expire after an 30mins to save space
        e = await self.generate_killmail_embed(losstype, killpackage)
        announce_chan = get(self.bot.get_all_channels(), id=channel)
        return await announce_chan.send(embed=e)

    async def generate_killmail_embed(self, losstype, killpackage) -> Embed:
        e = Embed(
            title=f"{losstype} Alert",
            url=f'https://zkillboard.com/kill/{killpackage["killmail_id"]}/',
        )
        if losstype == "Victim":
            e.colour = Color.dark_red()
        elif losstype == "Kill":
            e.colour = Color.green()
        elif losstype == "Big Kill":
            e.colour = Color.gold()

        e.set_thumbnail(
            url=f"https://imageserver.eveonline.com/Render/{killpackage['victim']['ship_type_id']}_64.png"
        )
        try:
            vic_name_sheet = await self.bot.esi.character_sheet(
                character_id=killpackage["victim"]["character_id"]
            )
        except KeyError:
            # no char id means a corp lost an asset, set name to blank for a cheap ez fix
            vic_name_sheet = {"name": ""}

        vic_corp_sheet = await self.bot.esi.corporation_sheet(
            corporation_id=killpackage["victim"]["corporation_id"]
        )

        affiliation_name = f"**{vic_corp_sheet['name']}**"
        if "alliance_id" in killpackage["victim"]:
            vic_alliance_sheet = await self.bot.esi.alliance_sheet(
                alliance_id=killpackage["victim"]["alliance_id"]
            )
            affiliation_name = (
                f"{vic_corp_sheet['name']}, **{vic_alliance_sheet['name']}**"
            )

        ship_info = await self.bot.esi.ship_info(
            ship_id=killpackage["victim"]["ship_type_id"]
        )

        # THREE ESI CALLS FOR REGION KILL ME
        sysinfo = await self.bot.esi.system_info(
            system_id=killpackage["solar_system_id"]
        )
        constellation = await self.bot.esi.constellation_info(
            constellation_id=sysinfo["constellation_id"]
        )
        region = await self.bot.esi.region_info(region_id=constellation["region_id"])

        e.description = "{victim} ({affiliation_info}) lost their {ship} in {system} ({region})".format(
            victim=vic_name_sheet["name"],
            affiliation_info=affiliation_name,
            ship=ship_info["name"],
            system=sysinfo["name"],
            region=region["name"],
        )

        for killer in killpackage["attackers"]:
            if killer["final_blow"] and "character_id" in killer:
                killer_name_sheet = await self.bot.esi.character_sheet(
                    character_id=killer["character_id"]
                )
                killer_corp_sheet = await self.bot.esi.corporation_sheet(
                    corporation_id=killer["corporation_id"]
                )

                affiliation_name = f"**{killer_corp_sheet['name']}**"
                if "alliance_id" in killer:
                    killer_alliance_sheet = await self.bot.esi.alliance_sheet(
                        alliance_id=killer["alliance_id"]
                    )
                    affiliation_name = f"{killer_corp_sheet['name']}, **{killer_alliance_sheet['name']}**"

                killer_ship_info = await self.bot.esi.ship_info(
                    ship_id=killer["ship_type_id"]
                )

                e.description += "\nFinal blow by {killer} ({affiliation_name}) flying a {ship}".format(
                    killer=killer_name_sheet["name"],
                    affiliation_name=affiliation_name,
                    ship=killer_ship_info["name"],
                )
                break

        e.add_field(
            name="Total Value", value=f'{killpackage["zkb"]["totalValue"]:,.2f}'
        )
        e.add_field(name="Total Attackers", value=str(len(killpackage["attackers"])))
        ts = pendulum.parse(killpackage['killmail_time'])
        #e.add_field(
            #text=f"killed {pendulum.parse(killpackage['killmail_time']).diff_for_humans()}"
        #    name="Made dead on",
        #    value=f"<t:{ts}>")
        e.timestamp = ts

        return e

    def __kill_key(self, kill_id, channel_id):
        hash = hashlib.md5()
        plain_key = "{}|{}".format(kill_id, channel_id)
        # hash.update(plain_key)
        # return hash.hexdigest()
        return plain_key

    @commands.group(pass_context=True)
    @commands.guild_only()
    async def killwatch(self, ctx):
        """
        Group of commands for starting and stopping active killwatches
        """
        if ctx.invoked_subcommand is None:
            return await ctx.send("Missing subcommand criteria for killwatching")

    @killwatch.command(hidden=True)
    @commands.is_owner()
    async def big(self, ctx, *, value: int):
        """
        Start or Stop a big value killwatch for current channel
        """
        await ctx.trigger_typing()

        result = await self.bot.redis.execute("get", "killwatch_criteria")
        if result:
            criteria = json.loads(result.decode("utf-8"))
            for item in list(criteria):
                if (
                    criteria[item]["channel"] == ctx.channel.id
                    and criteria[item]["scope"] == "Big Kill"
                ):
                    if value < 1:
                        del criteria[item]
                        await ctx.send(
                            f"Big Kill alert removed from #{ctx.channel.name}"
                        )
                    else:
                        criteria[item]["value"] = value
                        await ctx.send(
                            f"Big Kill Alert updated to value {value:,} for #{ctx.channel.name}"
                        )
                    await self.bot.redis.execute(
                        "set", "killwatch_criteria", json.dumps(criteria)
                    )
                    return

            if value < 1:
                return await ctx.send(
                    f"No Killwatch for #{ctx.channel.name} to remove."
                )
            criteria[len(criteria) + 1] = {
                "channel": ctx.channel.id,
                "id": None,
                "name": None,
                "value": value,
                "scope": "Big Kill",
            }
        else:
            criteria = {
                1: {
                    "channel": ctx.channel.id,
                    "id": None,
                    "name": None,
                    "value": value,
                    "scope": "Big Kill",
                }
            }
        await self.bot.redis.execute("set", "killwatch_criteria", json.dumps(criteria))
        await ctx.send(
            f"Big Kill alert added to #{ctx.channel.name} with value {value:,}"
        )

    async def process_entity_criteria(self, ctx, entity_type, entity_name, value):
        if entity_type not in ['character', 'corporation', 'alliance']:
            raise commands.BadArgument('Invalid entity!')

        try:
            entity_lookup = await self.bot.esi.search(entity_type, entity_name)
            entity_id = entity_lookup[entity_type][0]
        except KeyError:
            return await ctx.send(f"{entity_type.capitalize()} not found")

        if entity_type == 'character':
            entity_sheet = await self.bot.esi.character_sheet(character_id=entity_id)
        elif entity_type == 'corporation':
            entity_sheet = await self.bot.esi.corporation_sheet(corporation_id=entity_id)
        else:
            entity_sheet = await self.bot.esi.alliance_sheet(alliance_id=entity_id)

        result = await self.bot.redis.execute("get", "killwatch_criteria")
        if result:
            criteria = json.loads(result.decode("utf-8"))
            for item in list(criteria):
                if (
                    criteria[item]["channel"] == ctx.channel.id
                    and criteria[item]["id"] == entity_id
                ):
                    if value < 1:
                        del criteria[item]
                        await ctx.send(
                            f'{entity_type.capitalize()} Kill Alert removed for {entity_sheet["name"]} from #{ctx.channel.name}'
                        )
                    else:
                        criteria[item]["value"] = value
                        await ctx.send(
                            f'{entity_type.capitalize()} Kill Alert updated for {entity_sheet["name"]} to value {value:,} for #{ctx.channel.name}'
                        )
                    await self.bot.redis.execute(
                        "set", "killwatch_criteria", json.dumps(criteria)
                    )
                    return
            if value < 1:
                return await ctx.send(
                    f'No Killwatch for {entity_sheet["name"]} in #{ctx.channel.name} to remove.'
                )
            criteria[len(criteria) + 1] = {
                "channel": ctx.channel.id,
                "id": entity_id,
                "name": entity_sheet["name"],
                "value": value,
                "scope": entity_type,
            }
        else:
            criteria = {
                1: {
                    "channel": ctx.channel.id,
                    "id": entity_id,
                    "name": entity_sheet["name"],
                    "value": value,
                    "scope": entity_type,
                }
            }
        await self.bot.redis.execute("set", "killwatch_criteria", json.dumps(criteria))
        await ctx.send(
            f'Kill Alert added for {entity_sheet["name"]} to #{ctx.channel.name} with value {value:,}'
        )

    @killwatch.command(hidden=True)
    @commands.is_owner()
    async def alliance(self, ctx, alliance, value: int):
        """
        Start or Stop an alliance level killwatch with a given value for current channel
        """
        await ctx.trigger_typing()
        await self.process_entity_criteria(ctx, 'alliance', alliance, value)

    @killwatch.command(hidden=True)
    @commands.is_owner()
    async def corp(self, ctx, corp, value: int):
        """
        Start or Stop a corporation level killwatch with a given value for current channel
        """
        await ctx.trigger_typing()
        await self.process_entity_criteria(ctx, 'corporation', corp, value)

    @killwatch.command(hidden=True)
    @commands.is_owner()
    async def char(self, ctx, char, value: int):
        """
        Start or Stop a character level killwatch with a given value for current channel
        """
        await ctx.trigger_typing()
        await self.process_entity_criteria(ctx, 'character', char, value)

    @killwatch.command()
    async def list(self, ctx):
        """
        List current killwatches for current channel
        """
        result = await self.bot.redis.execute("get", "killwatch_criteria")
        if result:
            e = Embed(title=f"Active Killwatches for #{ctx.channel.name}")
            criteria = json.loads(result.decode("utf-8"))
            for item in criteria:
                if criteria[item]["channel"] == ctx.channel.id:
                    e.add_field(
                        name=criteria[item]["name"] or criteria[item]["scope"],
                        value=f'{criteria[item]["value"]:,}',
                    )
                    e.set_footer(
                        text=f"Currently fetching kills: {self.bot.zk.is_alive()}"
                    )
            return await ctx.send(embed=e)
        await ctx.send("There are no active killwatches for this channel")

    @killstream.before_loop
    async def before_killstream(self):
        await self.bot.wait_until_ready()

    @commands.Cog.listener()
    async def on_message(self, message):
        # need a permission system to delete other people's messages
        if not message.guild:
            return

        # we are only interested in single killmails for now, battle reports will still rely on zkill for the embed
        r = re.findall("https?://zkillboard.com/kill/\d+", message.content)
        if not r:
            return

        for found in r:
            report = await message.channel.send("Loading...")
            k = re.match(
                "(?P<url>https?://zkillboard.com)/kill/(?P<killid>\d+)/?", found
            )
            killid = k.group("killid").strip("/")

            try:
                # Fetch killmail information from zKill
                async with self.bot.session.get("https://zkillboard.com/api/killID/{}/".format(killid)) as resp:
                    text = await resp.text()
                    zkb = json.loads(text)[0]

                # Fetch killmail data from ESI
                km = await self.bot.esi.kill_lookup(kill_id=killid, kill_hash=zkb["zkb"]["hash"])

                km["zkb"] = zkb["zkb"]
                e = await self.generate_killmail_embed('Kill', km)
                e.colour = Color.from_rgb(0, 0, 0)  # make it black cuz why not
                await report.edit(content=None, embed=e)
                # if we dont suppress the original embed right away (before an embed exists maybe?)
                # then it shouldnt get suck being present and has the added bonus of keeping the original
                # embed should ours fail to generate
                try:
                    await message.edit(suppress=True)
                except Forbidden:
                    await report.edit(
                        content="To avoid the double post, give me the manage_messages permission "
                                "and I can suppress the original zkill embed :)",
                        embed=e,
                    )
            except:
                await report.delete()


def setup(bot):
    bot.add_cog(Killstream(bot))
