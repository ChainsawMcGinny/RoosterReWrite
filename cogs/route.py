import logging
import math
import traceback
from urllib.parse import quote
from urllib.error import HTTPError

from nextcord.ext import commands
from nextcord.embeds import Embed

log = logging.getLogger(__name__)


class Route(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def route(self, ctx, origin, destination, flag=None):
        """
        Find the shortest gate route between two systems with optional route type
        !route jita egghelende [shortest|insecure|secure]
        (if spaces use quotes for all)
        !route "jita" "old man star"
        """

        if flag:
            if flag not in ["shortest", "secure", "insecure"]:
                flag = "shortest"
        else:
            flag = "shortest"

        await ctx.trigger_typing()
        origin_lookup = await self.bot.esi.search("solar_system", origin)

        try:
            origin_id = origin_lookup["solar_system"][0]
        except KeyError:
            raise commands.BadArgument("Cannot find Origin system")

        destination_lookup = await self.bot.esi.search("solar_system", destination)
        try:
            desti_id = destination_lookup["solar_system"][0]
        except KeyError:
            raise commands.BadArgument("Cannot find Destination System")

        if origin_id == desti_id:
            return await ctx.send(
                "Do you really need directions if you are already there?"
            )

        routelist = await self.bot.esi.route(
            origin_id=origin_id, destination_id=desti_id, flag=flag
        )

        if routelist:
            routenames = await self.bot.esi.universe_names(
                ids=[routelist[0], routelist[-1]]
            )

            e = Embed()
            e.set_author(name=f"{flag} route: {len(routelist)-1} jumps")
            e.description = f'http://evemaps.dotlan.net/route/{quote(routenames[0]["name"])}:{quote(routenames[-1]["name"])}'
            return await ctx.send(embed=e)

    @commands.command(pass_context=True)
    async def ly(self, ctx, origin, destination):
        """
        Get the Lightyears distance between 2 systems
        """
        await ctx.trigger_typing()

        origin_lookup = await self.bot.esi.search("solar_system", origin)

        try:
            origin_id = origin_lookup["solar_system"][0]
        except KeyError:
            raise commands.BadArgument("Cannot find Origin system")

        destination_lookup = await self.bot.esi.search("solar_system", destination)
        try:
            desti_id = destination_lookup["solar_system"][0]
        except KeyError:
            raise commands.BadArgument("Cannot find Destination System")

        if origin_id == desti_id:
            return await ctx.send(
                "Do you really need directions if you are already there?"
            )

        origin_info = await self.bot.esi.system_info(system_id=origin_id)
        destination_info = await self.bot.esi.system_info(system_id=desti_id)

        or_x = origin_info["position"]["x"]
        or_y = origin_info["position"]["y"]
        or_z = origin_info["position"]["z"]

        de_x = destination_info["position"]["x"]
        de_y = destination_info["position"]["y"]
        de_z = destination_info["position"]["z"]

        dis_meters = math.sqrt(
            math.pow(or_x - de_x, 2)
            + math.pow(or_y - de_y, 2)
            + math.pow(or_z - de_z, 2)
        )
        dis_ly = dis_meters / 9.460_528_4e15

        return await ctx.send(
            f"{origin_info['name']} ⟷ {destination_info['name']}: {dis_ly:.3f} ly"
        )


def setup(bot):
    bot.add_cog(Route(bot))
