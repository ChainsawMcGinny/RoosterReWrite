import logging

from asyncio import sleep

from nextcord.ext import commands
from nextcord.utils import get

log = logging.getLogger(__name__)


class Join(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.Cog.listener()
    async def on_member_join(self, member):
        # sleep a few seconds, wait to see if they are granted any additional roles via auth before proceeding
        await sleep(10)
        if member.guild.id == 133405084410314752:
            channel = member.guild.system_channel

            if len(member.roles) > 1:
                if channel is not None:
                    return await channel.send(
                        f"Welcome back to ORGNC Public, {member.mention}"
                    )

            hr = get(member.guild.roles, name="Human Resources")
            if channel is not None:
                return await channel.send(
                    "Welcome to ORGNC Public chat, {0.mention}!  A {1.mention} member will be with"
                    " you shortly if you have any questions.  In the mean time, feel free to get to know us,"
                    " hang out, and if you are curious what else Rooster can do for you here, try using the "
                    " `!help` command!"
                    "".format(member, hr)
                )

    @commands.Cog.listener()
    async def on_member_remove(self, member):
        channel = member.guild.system_channel
        if channel is not None:
            return await channel.send(f"We will miss {member.display_name}...")


def setup(bot):
    bot.add_cog(Join(bot))
